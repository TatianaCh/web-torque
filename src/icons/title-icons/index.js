import edit_list from './edit_list.svg';
import sign from './sign.svg';
import glasses from './glasses.svg';
import wheel from './wheel.svg';
import engine from './engine.svg';
import key from './key.svg';
import carfront from './carfront.svg';

export {
    edit_list,
    sign,
    glasses,
    wheel,
    key,
    engine,
    carfront,
};
