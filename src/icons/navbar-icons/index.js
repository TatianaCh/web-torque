import car from './car.svg';
import eye from './eye.svg';
import flag from './flag.svg';
import json from './json.svg';
import calendar from './calendar.svg';
import double_wrench from './double-wrench.svg';
import logo_img from './logo_img.svg';
import logo_text from './logo_text.svg';
import twitter from './twitter.svg';
import facebook from './facebookcircle.svg';
import tool from './tool.svg';

export {
    car,
    eye,
    flag,
    json,
    calendar,
    double_wrench,
    logo_text,
    logo_img,
    facebook,
    twitter,
    tool,
};
