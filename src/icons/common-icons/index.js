import speed from './speed.svg';
import speed_empty from './speed empty.svg';
import pro_sign from './pro-sign.svg';
import upload from './upload.svg';
import chat_bubble from './chat-bubble.svg';
import notifications from './notifications.svg';

export {
    speed,
    speed_empty,
    pro_sign,
    upload,
    chat_bubble,
    notifications
};
