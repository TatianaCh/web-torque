/**
 * Default time to wait before key strokes
 * @type {Number}
 */
const WAIT_INTERVAL = 500;

/**
 * ASCII Code of the Enter key
 * @type {Number}
 */
const ENTER_KEY = 13;

/**
 * Build a simple event handler for a text field so that the onchange event is triggered only after a specific amount of
 * time. This is usefull for auto search text field, to avoid firing a bunch of queries on change.
 * @param  {function} action Method to execute when the onChange event is tigered.
 * @param  {Number} [interval=WAIT_INTERVAL] Time to wait inbetween key stroke
 * @return An object with two handlers that can be assigned to the TextField, one for onChange and one for onKeyDow
 */
export const changeTimer = (action, interval = WAIT_INTERVAL) => {
    // I we don't have a function, there's nothing to do.
    if (typeof action != 'function') {
        return;
    }

    // Initial a timer reference.
    let timer

    return {
        onChange: (event) => {
            clearTimeout(timer)
            const newValue = event.target.value
            timer = setTimeout(() => action(newValue), WAIT_INTERVAL)
        },
        onKeyDown: (event) => {
            if (event.keyCode === ENTER_KEY) {
                // If the user press the enter key, don't wait for the timeout. Just fire the event right away.
                clearTimeout(timer)
                action(event.target.value)
            }
        }
    }
}
