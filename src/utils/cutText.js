export default function cutText(text, length) {
	const cuttedText = text.substr(0, length);
	if (length < text.length) {
		return `${cuttedText}...`;
	}
    return cuttedText;
}
