import Moment from 'moment';
import { constants } from '../';

export function FormatDate(dateString) {
    return Moment(dateString).format(constants.Format.date);
}
