import { isNumeric } from './';

export default function getFormattedNumber(value, digits = 2) {
    let numberValue = isNumeric(value) ? parseFloat(value) : 0;
    let parts = numberValue.toFixed(digits).split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join('.');
}
