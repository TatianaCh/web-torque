import React from 'react'
import { Icon } from '../components/Icon/Icon'

/**
 * Given a list of props converts, will convert the `icon` attribute to an actual icon if it's a string.
 * @param  {object} props
 * @return {object}
 */
export default function string2Icon({icon, ...props}) {
    const iconComp =
        typeof icon == 'string' ?
        (<Icon name={icon} height={20} width={20} />) :
        icon

    return Object.assign({}, {icon: iconComp}, props)
}
