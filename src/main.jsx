import React from 'react';
import * as ReactDOM from 'react-dom';
import { Container, Row, Col } from 'reactstrap';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Navigation, Footer } from './components';
import ScrollToTop from './components/ScrollToTop';
import styles from './components/Layout/LargeSection.scss';
import { Home } from './containers/Home';
import { Login } from './containers/Login';
import { Register } from './containers/Register';
import { CarReviews } from './containers/CarReviews';
import { Workshop } from './containers/Workshop';
import { Drivers } from './containers/Drivers';
import { Events } from './containers/Events';
import { BuySell } from './containers/BuySell';
import { Dashboard } from './containers/Dashboard';

import users from '../demo/mocks/UsersResultSet.json';

import { IndividualCarModel } from './containers/IndividualCarModel';
import individualCarData from '../demo/mocks/IndividualCar.json';
import ImageGallery from '../demo/mocks/ImageGallery.json';

import { IndividualDriver } from './containers/IndividualDriver';
import individualDriverData from '../demo/mocks/IndividualDriver.json';

import { IndividualEvent } from './containers/IndividualEvent';
import individualEventData from '../demo/mocks/IndividualEvent.json';

import { IndividualWorkshop } from './containers/IndividualWorkshop';
import individualWorkshopData from '../demo/mocks/IndividualWorkshop.json';

import { IndividualSellingCar } from './containers/IndividualSellingCar';
import IndividualSellingCarData from '../demo/mocks/IndividualSellingCar.json';

import routes from './routes.json';
import subRoutes from './subRoutes.json';


const profile = users.items.length ? users.items[0] : {};

ReactDOM.render(
    <Router>
        <ScrollToTop>
            <Navigation routes={routes} subRoutes={subRoutes} profile={profile} useRouter />
            <Container className={styles.LargeSection}>
                <Route exact path="/" component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/signup" component={Register} />
                <Route exact path="/car-reviews" component={CarReviews} />
                <Route exact path="/workshop" component={Workshop} />
                <Route exact path="/drivers" component={Drivers} />
                <Route exact path="/events" component={Events} />
                <Route exact path="/buysell" component={BuySell} />
                <Route exact path="/dashboard" component={Dashboard} />
                <Route path="/dashboard/:section" component={Dashboard} />
                <Route path="/car-model/:id" render={(props) => (<IndividualCarModel data={{item: individualCarData}} ImageGallery={ImageGallery} {...props} />)} />
                <Route path="/driver/:id" render={(props) => (<IndividualDriver data={{item: individualDriverData}} {...props} />)} />
                <Route path="/event/:id" render={(props) => (<IndividualEvent data={{item: individualEventData}} {...props} />)} />
                <Route path="/workshop/:id" render={(props) => (<IndividualWorkshop data={{item: individualWorkshopData}} {...props} />)} />
                <Route path="/buy/:id" render={(props) => (<IndividualSellingCar data={{item: IndividualSellingCarData}} {...props} />)} />
            </Container>
            <Footer routes={routes} useRouter />
        </ScrollToTop>
    </Router>
    , document.getElementById('root')
);
