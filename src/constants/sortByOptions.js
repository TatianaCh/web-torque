export const sortByOptions = [
    { label: "Rating", sort: "rating" },
    { label: "Price", sort: "price" },
    { label: "Name", sort: "name" },
    { label: "Date", sort: "date" },
];
