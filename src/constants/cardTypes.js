export const cardTypes = {
    'events': 'events',
    'carModel': 'carModel',
    'driver': 'driver',
    'workshop': 'workshop',
    'carReviews': 'carReviews',
    'buySell': 'buySell',
    'viewAllLink': 'viewAllLink',
}
