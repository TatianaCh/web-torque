export const carCharacteristicLabels = {
    "hotness": "Hotness",
    "performance": "Performance",
    "safety": "Safety",
    "price": "Price",
    "fuel": "Fuel Consumption",
    "comfort": "Comfort",
};
