import React from 'react';
import styles from './LoginSocial.scss';

const SocialForm = () => {
    return (
        <div className={styles.SocialForm}>
            <h2 className={styles.heading}>SOCIAL LOGIN</h2>
            <h4 className={styles.account}>Use a social account to login to smartcookie8</h4>
            <button className={styles.button}>LOGIN WITH FACEBOOK</button>
            <button className={styles.button}>LOGIN WITH GOOGLE</button>
        </div>
    );
};

export default SocialForm;
