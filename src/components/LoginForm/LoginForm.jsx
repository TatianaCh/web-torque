import React from 'react';
import { Container, Input } from 'reactstrap';
import styles from './Login.scss';
import { LabelledField } from '../';
import LoginSocial from './LoginSocial';


export const LoginForm = () => {
    return (
        <section>
            <Container className={styles.Login}>
                <div className={styles.form}>
                    <div className={styles.contentForm}>
                        <div className={styles.aside}>
                            <h2 className={styles.heading}>LOGIN</h2>
                            <LabelledField label="EMAIL ADDRESS" id="email" className={styles.info}>
                                <Input type="text" id="email" />
                            </LabelledField>
                            <LabelledField label="PASSWORD" id="password" className={styles.info}>
                                <Input type="password" id="password" />
                            </LabelledField>
                            <a className={styles.pass} href='#'>Forgot password?</a>
                            <button className='btn btn-bordered'>LOGIN</button>
                            <div className={styles.smart}>
                                New to Smart Cookie?
                                <a className={styles.sign} href='/signup'>Sign Up</a>
                            </div>
                        </div>
                    </div>
                    <LoginSocial />
                </div>
            </Container>
        </section>
    );
};
