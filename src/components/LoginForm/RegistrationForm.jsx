import React from 'react';
import { Container, Input } from 'reactstrap';
import styles from './Login.scss';
import { LabelledField } from '../';
import LoginSocial from './LoginSocial';

export const RegistrationForm = () => {
    const fields = {
        'first_name': { label: 'FIRST NAME' },
        'last_name': { label: 'LAST NAME' },
        'public_name': { label: 'PUBLIC FACING NAME' },
        'email': { label: 'EMAIL ADDRESS' },
        'password': { label: 'PASSWORD', type: 'password' },
        'password2': { label: 'CONFIRM PASSWORD', type: 'password' },
    };
    const fieldsList = Object.keys(fields).map(key => {
        const inputType = fields[key].type || 'text';
        return (
            <LabelledField label={fields[key].label} id={key} className={styles.info} key={key}>
                <Input type={inputType} id={key} />
            </LabelledField>
        );
    })
    return (
        <section>
            <Container className={styles.Login}>
                <div className={styles.form}>
                    <div className={styles.contentForm}>
                        <div className={styles.section}>
                            <h2 className={styles.heading}>REGISTER</h2>
                            <h4 className={styles.join}>Join smartcookie8</h4>
                            {fieldsList}
                            <h5 className={styles.terms}>
                                <input className={styles.termsCheckbox} type="checkbox" id="terms-cb"/>
                                <label htmlFor="terms-cb">I have read and accept the Smart Cookie terms and coundtions.</label>
                            </h5>
                            <button className='btn btn-bordered'>REGISTER</button>
                        </div>
                    </div>
                    <LoginSocial />
                </div>
            </Container>
        </section>
    );
};
