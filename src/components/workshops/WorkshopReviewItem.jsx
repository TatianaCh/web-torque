import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import moment from 'moment';
import { CardText, Card } from 'reactstrap';
import { FormatDate, isEmptyObject, cutText } from '../../utils';
import { Icon, RatingValue } from '../';
import style from './WorkshopReviewItem.scss';


export class WorkshopReviewItem extends React.Component {
    render() {
        const { data } = this.props;
        const reviewDate = moment(data.Created).format('MMMM Do, YYYY');
        let userInfoBlock = (<div className={style.userInfo} />);
        if (!isEmptyObject(data.user)) {
            userInfoBlock = (
                <div className={style.userInfo}>
                    <div className={style.userAvatar}>
                        <img src={data.user.PreviewThumbnailURL} />
                    </div>
                    <div className={style.userData}>
                        <div className={style.userName}>{data.user.Name}</div>
                    </div>
                </div>
            );
        }
        const isMore = data.Text.length > 220;
        const rating = data.Rating || 0;
        return (
            <Card className={style.reviewCard}>
                {userInfoBlock}
                <div className={style.reviewBody}>
                    <div className={style.rating}>
                        <RatingValue value={rating} />
                    </div>
                    <CardText className={style.text}>
                        {cutText(data.Text, 220)}
                    </CardText>
                    <div className={style.reviewTime}>
                        <div className={style.date}>{reviewDate}</div>
                        {isMore && (<a className={style.readMore}>More details</a>)}
                    </div>
                </div>
            </Card>
        );
    }
}

WorkshopReviewItem.defaultProps = {
    data: {},
};

WorkshopReviewItem.propTypes = {
    data: PropTypes.object,
};
