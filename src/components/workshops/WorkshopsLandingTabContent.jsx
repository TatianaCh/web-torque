import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col,Container } from 'reactstrap';
import { SectionHeader, TabList, Tab, CardListHOC } from '../';
import { Icon, SearchWorkshops, SimpleSearchForm } from '../';
import { cardTypes } from '../../constants';

const hero = './images/workshop.png';

const titleImage = (<img src={hero} alt="" />);



export class WorkshopsLandingTabContent extends React.Component {
    constructor(props) {
        super(props);
        this.onSearchHandler = this.onSearchHandler.bind(this);
        this.searchBlockData = {
            types: { ...props.dictionaries.service },
            places: { ...props.dictionaries.region },
            years: { ...props.dictionaries.ratings },
        };
        this.state = {
            isSearch: false,
        };
    }

    onSearchHandler() {
        this.setState({
            isSearch: true,
        });
    }

    render() {
        const { isSearch } = this.state;
        const { dictionaries, data, rData, cardType } = this.props;
        const searchBlock = (<SimpleSearchForm dropdowns={this.searchBlockData} onSearchHandler={this.onSearchHandler} />);
        if (isSearch) {
            return (
                <div>
                    {searchBlock}
                    <Container>
                        <SearchWorkshops
                            {...dictionaries}
                            data={data}
                            cardType={cardType}
                            limit={4}
                            currentPage={8}
                            totalPage={10}
                            totalCount={37}
                            offset={32} />
                        />
                    </Container>
                </div>
            );
        }
        return (
            <div>
                {searchBlock}
                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children='MECHANICAL WORKSHOPS' hero={titleImage} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'TYRES'} hero={titleImage} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} cardType={cardType} currentPage={1}  />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'PANEL AND PAINT SHOPS'} hero={titleImage} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} cardType={cardType} currentPage={2} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType}  currentPage={3} />
                    </Tab>
                </TabList>
            </div>
        );
    }
}

WorkshopsLandingTabContent.propTypes = {
    dictionaries: PropTypes.object,
    cardType: PropTypes.string.isRequired,
    data: PropTypes.object,
    rData: PropTypes.object,
};

WorkshopsLandingTabContent.defaultProps = {
    dictionaries: {},
    data: {},
    rData: {},
};
