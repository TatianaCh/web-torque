import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import moment from 'moment';
import s from './AboutWorkshop.scss';
import { GoogleMapComponent, WorkshopReviewsListHOC, RatingValue } from '../';

export class AboutWorkshop extends React.Component {
    render() {
        const { data, workshopServices, reviews } = this.props;
        const schedule = data.Schedule.map((item) => {
            let timeInfo = "Closed";
            if (item.isOpen !== false) {
                const dateFrom = moment(item.From, 'h:mm');
                const dateTo = moment(item.To, 'h:mm');
                timeInfo = `${dateFrom.format('hA')} to ${dateTo.format('hA')}`;
            }
            return (
                <div key={item.Day} className={s.workshopInfoItem}>
                    <span>{moment().weekday(item.Day).format('dddd')}</span>
                    <span>{timeInfo}</span>
                </div>
            );
        });
        const servicesList = [];
        if (data.Services && data.Services.length) {
            data.Services.forEach((item) => {
                const serviceName = workshopServices.find(servItem => (servItem.ID === item));
                if (serviceName) {
                    servicesList.push(
                        <span key={serviceName.ID} className={s.serviceTag}>{serviceName.Name}</span>
                    );
                }
            });
        }
        const reviewsData = reviews.map(review => ({ data: { ...review }, ID: review.ID }));
        return (
            <div className={s.aboutBlock}>
                <div className={s.contentBlock}>
                    <div className={s.servicesBlock}>
                        <div className={s.description} dangerouslySetInnerHTML={{__html: data.Description}} />
                        <div className={s.subTitle}>services offered</div>
                        <div className={s.servicesContainer}>{servicesList}</div>
                    </div>
                    <div className={s.overviewBlock}>
                        <div className={s.subTitle}>overview</div>
                        <div className={s.ratingBlock}>
                            <RatingValue value={data.Rating} heightIcons={40} widthIcons={55} />
                            <span className={s.ratingSpan}>{data.Rating}</span>
                        </div>
                        <div className={s.ratingInfo}>
                            <div className={s.costBlock}>
                                <RatingValue value={data.Rating} heightIcons={24} widthIcons={33} />
                                <span>Quality of Work</span>
                                <RatingValue value={data.Rating} heightIcons={24} widthIcons={33} />
                                <span>Friendliness of Staff</span>
                            </div>
                            <div className={s.costBlock}>
                                <RatingValue value={data.Rating} heightIcons={24} widthIcons={33} />
                                <span>Cost</span>
                                <RatingValue value={data.Rating} heightIcons={24} widthIcons={33} />
                                <span>Speed of Repair</span>
                            </div>
                        </div>
                    </div>
                    <div className={s.reviewsBlock}>
                        <div className={s.buttonBlock}>
                            <h3>Individual Reviews ({reviews.length})</h3>
                            <Button color="primary">WRITE A REVIEW</Button>
                        </div>
                        <div className={s.reviewsList}>
                            <WorkshopReviewsListHOC data={{ items: reviewsData }} itemClassName={s.reviewCard} lg={12} md={12} sm={12} xs={12} />
                        </div>
                    </div>
                </div>
                <div className={s.barBlock}>
                    <div className={s.mapBlock}>
                        <div className={s.map}>
                            <GoogleMapComponent
                                text={data.Location.address}
                                lat={data.Location.lat}
                                lon={data.Location.lon}
                            />
                        </div>
                        <h2>location name</h2>
                        <div className={s.subTitle}>{data.locations} location</div>
                    </div>
                    <div className={s.contactBlock}>
                        <div className={s.subTitle}>CONTACT</div>
                        <div className={s.addressBlock}>
                            <div className={s.workshopInfoItem}>{data.Contacts.phone}</div>
                            <div className={s.workshopInfoItem}>{data.Contacts.url}</div>
                            <div className={s.workshopInfoItem}>{data.Contacts.email}</div>
                        </div>
                        <div>
                            <div className={s.workshopInfoItem}><a href={data.Contacts.facebook}>Facebook</a></div>
                            <div className={s.workshopInfoItem}><a href={data.Contacts.youtube}>Youtube</a></div>
                            <div className={s.workshopInfoItem}><a href={data.Contacts.instagram}>Instagram</a></div>
                        </div>
                    </div>
                    <div className={s.scheduleBlock}>
                        <div className={s.subTitle}>Opening hours</div>
                        {schedule}
                    </div>
                </div>
            </div>
        );
    }
}

AboutWorkshop.defaultProps = {
    data: {},
    reviews: [],
    workshopServices: [],
};

AboutWorkshop.propTypes = {
    data: PropTypes.object,
    reviews: PropTypes.array,
    workshopServices: PropTypes.array,
};
