import React from 'react';
import { Icons } from '../..';
import styles from './Icon.scss';

const fallback = 'json'

export const Icon = ({name, color, className2, ...args}) => {
	let iconClass = styles['Icon'];
	if (className2) {
		iconClass += ` ${className2}`;
	}
	if (styles[color]) {
		iconClass += ` ${styles[color]}`;
	}

    let iconComponent = Icons[name];
    if (iconComponent === undefined) {
        console.warn(`Can't find the ""${name}" icon.`);
        iconComponent = Icons[fallback];
    }

    const props = Object.assign(
        {},
        args,
        { className: iconClass },
    );
    return React.createElement(iconComponent, props);
}

Icon.defaultProps = {
    color: '',
}
