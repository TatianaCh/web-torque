import React from 'react';
import PropTypes from 'prop-types';
import lorem from 'lorem-ipsum';
import * as Rambda from 'rambda';
import { Row, Col, Button } from 'reactstrap';
import { Icon, ImagesListHOC, VideosListHOC } from '../../';
import styles from './AboutCar.scss';


export class AboutCar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data: {AttributionUrl, Description}, onShowImg, ImageGallery } = this.props;

        const imgSrc = "https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180";
        const videosData = Rambda.times( i => (
            {
                ID: i,
                preview: imgSrc,
                name: 'Video',
                time: '00:00',
                views: 123,
            }
        ), 4);

        return (
            <div>
                <Row className={styles.AboutBlock}>
                    <Col lg='6' xs='12' className={styles.contentBlock}>

                        <div dangerouslySetInnerHTML={{__html: Description}} />

                        <div className={styles.note}>
                            ORIGINAL CONTENT PROVIDER BY WIKIPEDIA <a href={AttributionUrl}>https://www.wikipedia.org/</a>
                        </div>

                    </Col>
                    <Col lg='6' xs='12' className={styles.imagesBlock}>
                        <div className={styles.imagesItem}>
                            <div className={styles.titleBlock}>
                                <span className='title title--small'>IMAGES</span>

                                <span className='upload'>
                                    <input type="file"/>
                                    <label>UPLOAD AN IMAGE <Icon name='upload' height={20} width={20} /></label>
                                </span>
                            </div>
                            <div>
                                <ImagesListHOC {...ImageGallery} onShow={onShowImg} itemClassName={styles.carImg} lg={6} md={6} sm={12} xs={12} showBorder={false} />
                            </div>
                        </div>
                        <div className={styles.imagesItem}>
                            <div className={styles.titleBlock}>
                                <span className='title title--small'>User videos</span>

                                <span className='upload'>
                                    <input type="file"/>
                                    <label>UPLOAD AN IMAGE <Icon name='upload' height={20} width={20} /></label>
                                </span>
                            </div>
                            <div>
                                <VideosListHOC data={{ items: videosData }} itemClassName={styles.carImg} lg={6} md={6} sm={12} xs={12} />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

AboutCar.defaultProps = {
    data: {},
};

AboutCar.propTypes = {
    data: PropTypes.object,
};
