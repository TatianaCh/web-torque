import React from 'react';
import PropTypes from 'prop-types';
import lorem from 'lorem-ipsum';
import { Row, Col, Button } from 'reactstrap';
import { ReviewItem, Chart, RatingValue, Icon, ReviewsListHOC } from '../../';
import { carCharacteristicLabels } from '../../../constants';
import style from './CarModelReviews.scss';


export class CarModelReviews extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { reviews, rating, reviewCount, reviewsDistribution, charactericsEsteem } = this.props;

        const reviewsData = reviews.map(review => ({ data: { ...review }, ID: review.ID }));

        const charactericsEsteemList = Object.keys(charactericsEsteem).map(key => (
            <Col md={6} sm={12} xs={12} className={style.overallRow} key={key}>
                <RatingValue value={charactericsEsteem[key]} />
                <div className={style.ratingText}>{carCharacteristicLabels[key]}</div>
            </Col>
        ));
        return (
            <div>
                <Row className={style.overallRatingContainer}>
                    <Col lg={6} md={12} className={style.overallRatingItem}>
                        <div className={style.overallRatingValue}>
                            <div className={style.value}>
                                <div>{rating.toFixed(1)}</div>
                                <Icon name="speed" color="blue" height={20} />
                            </div>
                            <div className={style.subTitle}>OVERALL</div>
                            <div className={style.review}>{reviewCount} pro reviews</div>
                        </div>
                        <div className={style.chartContainer}>
                            <Chart data={reviewsDistribution} reviewCount={reviewCount} />
                        </div>
                    </Col>
                    <Col lg={6} md={12} className={style.overallRatingItem}>
                        <div className={style.characteristics}>
                            <div className={style.overallTitle}>Over all pro scores</div>
                            <Row>
                                {charactericsEsteemList}
                                <Col md={6} sm={12} xs={12} className={style.overallRow}>
                                    <Button color="primary" className={style.Button}>View all</Button>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                <div className={style.reviewsList}>
                    <ReviewsListHOC data={{ items: reviewsData }} itemClassName={style.reviewCard} lg={6} md={6} sm={12} xs={12} />
                </div>
            </div>
        );
    }
}

CarModelReviews.defaultProps = {
    reviews: [],
    reviewsDistribution: {},
    charactericsEsteem: {},
};

CarModelReviews.propTypes = {
    reviews: PropTypes.array,
    rating: PropTypes.number.isRequired,
    reviewCount: PropTypes.number.isRequired,
    reviewsDistribution: PropTypes.object,
    charactericsEsteem: PropTypes.object,
};
