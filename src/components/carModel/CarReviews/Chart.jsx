import React from 'react';
import PropTypes from 'prop-types';
import { Progress  } from 'reactstrap';
import { isEmptyObject } from '../../../utils';
import style from './Chart.scss';


export class Chart extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data, reviewCount } = this.props;
        if (isEmptyObject(data)) {
            return false;
        }
        const dataArray = Object.keys(data).map(key => ({ label: key, value: (data[key] * 100) / reviewCount }));
        dataArray.sort((val1, val2) => {
            return parseFloat(val2.label) - parseFloat(val1.label);
        });
        const charts = dataArray.map(item => (
            <div key={item.label} className={style.chartItem}>
              <div className={style.property}>{item.label}/5</div>
              <Progress value={item.value} />
              <div className={style.value}>100%</div>
            </div>
        ));
        return charts;
    }
}

Chart.defaultProps = {
    data: {},
    reviewCount: 0,
};

Chart.propTypes = {
    data: PropTypes.object,
    reviewCount: PropTypes.number,
};
