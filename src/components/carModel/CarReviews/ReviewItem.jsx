import React from 'react';
import PropTypes from 'prop-types';
import lorem from 'lorem-ipsum';
import { Row, Col, CardText, Card } from 'reactstrap';
import { FormatDate, isEmptyObject, cutText } from '../../../utils';
import { Icon, RatingValue } from '../../';
import style from './ReviewItem.scss';


export class ReviewItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data } = this.props;
        const reviewDate = FormatDate(data.Created);
        let userInfoBlock = (<div className={style.userInfo} />);
        if (!isEmptyObject(data.user)) {
            const userAccCreated = FormatDate(data.user.Created);
            const userLocation = [data.user.City, data.user.Country].join(', ');
            const proSign = (
                <div className={style.proSign}>
                    <Icon name="pro_sign" color="blue" width="14" />
                    <span>Pro review</span>
                </div>
            );
            userInfoBlock = (
                <div className={style.userInfo}>
                    <div className={style.userAvatar}>
                        <img src={data.user.PreviewThumbnailURL} />
                    </div>
                    <div className={style.userData}>
                        <div className={style.userName}>{data.user.Name}</div>
                        <div className={style.userLocation}>{userLocation}</div>
                        <div className={style.userAccCreated}>{userAccCreated}</div>
                        {data.user.isPro && proSign}
                    </div>
                </div>
            );
        }
        const isMore = data.Text.length > 220;
        const rating = data.Rating || 0;
        return (
            <Card className={style.reviewCard}>
                {userInfoBlock}
                <div className={style.reviewBody}>
                    <div className={style.rating}>
                        <RatingValue value={rating} />
                        <div className={style.ratingText}><span>{rating}</span> overall</div>
                    </div>
                    <CardText className={style.text}>
                        {cutText(data.Text, 220)}
                    </CardText>
                    {isMore && (<a className={style.readMore}>Read more</a>)}
                    <div className={style.reviewTime}>{reviewDate}</div>
                </div>
            </Card>
        );
    }
}

ReviewItem.defaultProps = {
    data: {},
};

ReviewItem.propTypes = {
    data: PropTypes.object,
};
