import React from 'react';
import PropTypes from 'prop-types';
import lorem from 'lorem-ipsum';
import { Button, Container, Row, Col } from 'reactstrap';
import { ReviewItem, CardListItem, List } from '../../';
import { cardTypes } from '../../../constants';
import style from './CarModelBuySell.scss';

const cardType = cardTypes.buySell;

export class CarModelBuySell extends React.Component {
    constructor(props) {
        super(props);
        this.data = { ...props.data };
        const { carMake, carModel } = props;
        const viewAllLabel = props.isParts ? `View all parts for ${carMake}, ${carModel}` : `View all listings for ${carMake}, ${carModel}`;
        const items = [...props.data.items];
        if (props.showViewAllButton) {
            items.push({
                ID: 'view-all-card',
                cardType: cardTypes.viewAllLink,
                label: viewAllLabel,
                Url: '/buysell',
            });
        };
        this.data.items = items;
    }

    render() {
        const { isParts, carMake } = this.props;
        const list = List(CardListItem)({ data: this.data, cardType, lg: 3, md: 4, sm: 6, xs: 12 });

        let title = isParts ? 'parts for sale for this vehicle' : `${carMake}, MODELS FOR SALE`;
        if (!carMake) {
            title = isParts ? 'parts for sale' : 'MODELS FOR SALE';
        }
        const titleButton = isParts ? 'list a part' : 'list a car';
        return (
            <div>
                <div className={style.topPanel}>
                    <h4 className="title">{title}</h4>
                    <Button color="primary" size="lg" outline>{titleButton}</Button>
                </div>
                {list}
            </div>
        );
    }
}

CarModelBuySell.defaultProps = {
    data: { items: [] },
    carMake: '',
    carModel: '',
    isParts: false,
    showViewAllButton: true,
};

CarModelBuySell.propTypes = {
    data: PropTypes.object,
    carMake: PropTypes.string,
    carModel: PropTypes.string,
    isParts: PropTypes.bool,
    showViewAllButton: PropTypes.bool,
};
