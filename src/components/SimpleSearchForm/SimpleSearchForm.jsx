import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import style from './SimpleSearch.scss';
import { Container, Button, Form, Input } from 'reactstrap';
import { DropDown } from '../';

const PureSimpleSearchForm = ({dropdowns, showSearchInput, onSearchHandler}) => (
    <Form inline className={style.form}>
            <Container>
    	{showSearchInput && (<div className={style.searchMargin}><Input className={style.search} type="search" name="search" placeholder="Search" /></div>)}

        {Object.keys(dropdowns).map(key => (
            <DropDown key={key} label={dropdowns[key].label} name={key} labelHidden items={dropdowns[key].options} placeholder={dropdowns[key].placeholder} />
        ))}
        <Button color='primary' className={style.searchButton} onClick={onSearchHandler}>Go!</Button>
            </Container>
    </Form>
);

PureSimpleSearchForm.defaultProps = {
    dropdowns: {},
    showSearchInput: false,
    onSearchHandler: null,
}


export const SimpleSearchForm = compose(
    pure,
)(PureSimpleSearchForm);
