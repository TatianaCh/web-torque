import React from 'react';
import PropTypes from 'prop-types';
import * as Rambda from 'rambda';
import { Container, Button as BootstrapButton } from 'reactstrap';
import { DropDown, AccordionComponent, Button } from '../';
import { sortByOptions } from '../../constants';
import styles from './FilterList.scss';

const priceOptions = Rambda.times( i => (
    { value: i * 1000, label: i * 1000 }
), 20);
const dropDownProps = {
    valueField: 'value',
    displayField: 'label',
    className: styles.dropDown,
    items: priceOptions,
    labelHidden: true,
}


export const FilterList = ({filterOptions, filterKey, onSearchReset, filterFormName}) => (
<div className={styles.FilterList}>
    <div className={styles.titleBlock}>
        <h2 className={styles.title}>REFINE SEARCH</h2>
        <Button className={styles.reset} onClick={onSearchReset} color="link">RESET</Button>
    </div>
    <div className={styles.titleBlock}>
        <h4 className={styles.subTitle}>Select the rating parameters for each category</h4>
    </div>
    <div className={styles.buttonBlock}>
        <BootstrapButton color="blue" className={styles.addButton}>ADVANCED FILTERS</BootstrapButton>
    </div>
    <form name={filterFormName}>
        {filterOptions.map(({label, field, forceOpen}) => (
            <AccordionComponent forceOpen={forceOpen} className2={styles.filterAccordion} label={label} key={`${label}_${filterKey}`}>
                <div>
                    {field}
                </div>
            </AccordionComponent>
        ))}
    </form>

</div>
)

FilterList.propTypes = {
    filterOptions: PropTypes.array,
    filterKey: PropTypes.string,
}

FilterList.defaultProps = {
    filterOptions: [],
    filterKey: '',
}
