import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Container, Button } from 'reactstrap';
import { Icon } from '../../';
import { getFormattedNumber } from '../../../utils';
import styles from './ShortListingInfo.scss';


export class ShortListingInfo extends React.Component {
    render() {
        const { data, onShowImg } = this.props;
        const closeDateFormat = 'ha, dddd, Do MMMM';
        return (
            <div className={styles.ImgSection}>
                <div className={styles.imgBlock}>
                    <div className={styles.imgAvatar}>
                        <img src={data.user.PreviewThumbnailURL} />
                    </div>
                    <div className={styles.imgData}>
                        <div className={styles.imgName}>{data.user.Name}</div>
                        <div className={styles.imgTown}>{data.user.City}, {data.user.Country}</div>
                        <div className={styles.imgRating}>
                            <Icon color="blue" height={15} width={15} name='edit_list' />
                            <span className={styles.raitingEdit}>{data.user.ListingsCounter}</span>
                            <Icon color="blue" height={14} width={20} name='speed' />
                            <span>{data.user.ReviewsCounter}</span>
                        </div>
                    </div>
                </div>
                <div className={styles.imgInfo}>
                    <span className={styles.infoSpan}>
                        listing #: {data.Number} closes {moment(data.End).format(closeDateFormat)}
                    </span>
                    <div className={styles.infoCont}>
                        <div className={styles.infoPhone}>
                            <div className={styles.infoPrice}>
                                <h3>asking price</h3>
                                <span>$ {getFormattedNumber(data.Price, 0)} ono</span>
                            </div>
                            <h3>daytime phone</h3>
                            <span>{data.Contacts.phone}</span>
                        </div>
                        <div className={styles.infoEmail}>
                            <div className={styles.infoPrice}>
                                <h3>listing closes</h3>
                                <span>{moment(data.End).format(closeDateFormat)}</span>
                            </div>
                            <h3>email</h3>
                            <span>{data.Contacts.email}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ShortListingInfo.defaultProps = {
    data: {},
};

ShortListingInfo.propTypes = {
    data: PropTypes.object,
};
