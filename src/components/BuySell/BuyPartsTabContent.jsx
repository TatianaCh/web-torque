import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col,Container } from 'reactstrap';
import { SectionHeader, TabList, Tab, CardListHOC } from '../';
import { Icon, SearchBuySell, SimpleSearchForm } from '../';
import { cardTypes } from '../../constants';

const hero = './images/car-frame.png';

const titleImage = (<img src={hero} alt="" />);


export class BuyPartsTabContent extends React.Component {
    constructor(props) {
        super(props);
        this.onSearchHandler = this.onSearchHandler.bind(this);
        this.state = {
            isSearch: false, // when state true, appears search result.Happens when you press the button "GO!"
        };
    }

    onSearchHandler() {
        this.setState({
            isSearch: true,
        });
    }

    render() {
        const { isSearch } = this.state;
        const { dictionaries, data, rData, cardType } = this.props;
        const searchBlock = (<SimpleSearchForm dropdowns={dictionaries} onSearchHandler={this.onSearchHandler} showSearchInput />);
        if (isSearch) {
            return (
                <div>
                    {searchBlock}
                    <Container>
                        <SearchBuySell
                            {...dictionaries}
                            data={data}
                            cardType={cardType}
                            limit={4}
                            currentPage={8}
                            totalPage={10}
                            totalCount={37}
                            offset={32} />
                        />
                    </Container>
                </div>
            );
        }
        return (
            <div>
                {searchBlock}
                <SectionHeader icon={<Icon name='wheel' height={36} width={36} />} children='TOP LISTED ENGINE PARTS' hero={titleImage} cardType={cardType} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='wheel' height={36} width={36} />} children='TOP LISTED BODY PARTS' hero={titleImage} cardType={cardType} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={1} cardType={cardType}  />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='wheel' height={36} width={36} />} children='TOP LISTED WHEELS & TYRES' hero={titleImage} cardType={cardType} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={2} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} currentPage={3} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='wheel' height={36} width={36} />} children='TOP LISTED PERFORMANCE PARTS' hero={titleImage} cardType={cardType} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={2} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} currentPage={3} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='wheel' height={36} width={36} />} children='TOP LISTED GENERAL PARTS' hero={titleImage} cardType={cardType} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={2} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} currentPage={3} cardType={cardType} />
                    </Tab>
                </TabList>
            </div>
        );
    }
}

BuyPartsTabContent.propTypes = {
    dictionaries: PropTypes.object,
    cardType: PropTypes.string.isRequired,
    data: PropTypes.object,
    rData: PropTypes.object,
};

BuyPartsTabContent.defaultProps = {
    dictionaries: {},
    data: {},
    rData: {},
};
