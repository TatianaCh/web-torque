import React from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'reactstrap';
import styles from './Questions.scss';


export class AskQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.maxLength = 700;
        this.state = {
            text: '',
        };
    }

    onChangeHandler(event) {
        this.setState({
            text: event.target.value.substr(0, this.maxLength),
        });
    }

    render() {
        const { text } = this.state;
        return (
            <div className={styles.infoBlock}>
                <span className={styles.title}>ask the seller a question</span>
                <div className={styles.infoCont}>
                    <p>To prevent aute eiusmod dolore cillum id voluptate aliqua.
                    Reprehenderit esse consequat qui quis occaecat ex proident ea officia
                    dolor ipsum proident sunt magna. Duis magna irure eiusmod ea aliquip irure elit.
                    Nisi duis sit tempor sunt anim incididunt qui ut.
                    </p>
                </div>
                <span className={styles.count}>{this.maxLength - text.length}</span>
                <Input type="textarea" onChange={this.onChangeHandler} value={text} />
                <div className={styles.buttonBlock}>
                    <Button color="primary" className={styles.button}>Go!</Button>
                </div>
            </div>
        );
    }
}
