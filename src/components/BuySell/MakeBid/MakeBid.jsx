import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Input, Container, Button } from 'reactstrap';
import { Icon, ShortListingInfo } from '../../';
import { getFormattedNumber } from '../../../utils';
import styles from './MakeBid.scss';


export class MakeBid extends React.Component {
    render() {
        const { data, onShowImg } = this.props;
        const bids = [...data.bids];
        bids.sort((item1, item2) => (moment(item2.Date).diff(moment(item1.Date))));
        const bidsList = bids.map((item) => {
            return (
                <tr key={item.ID}>
                    <td className={styles.name}>{item.user.Name}</td>
                    <td className={styles.icon}><Icon color="blue" height={20} width={20} name='speed' /><span>{item.user.ReviewsCounter}</span></td>
                    <td className={styles.date}>{moment(item.date).format('hh:mm A, ddd D MMM')}</td>
                    <td className={styles.price}>$ {getFormattedNumber(item.Bid, 0)}</td>
                </tr>
            );
        });
        return (
            <Container className={styles.AboutBlock}>
                <div className={styles.EventsConBlock}>
                    <span className={styles.title}>make a bid</span>
                    <span className={styles.subTitle}>bid history</span>
                    <table className={`gradient-table ${styles.details}`}>
                        <tbody>
                            {bidsList}
                        </tbody>
                    </table>
                    <span className={styles.subTitle}>your bid</span>
                    <div className={styles.inputBlock}><Input type="number" /><Button color="primary" className={styles.button}>SUBMIT</Button></div>
                </div>

                <div className={styles.infoBlock}>
                    <div>
                        <span className={styles.title}>make an offer</span>
                        <span className={styles.subTitle}>your bid</span>
                        <div className={styles.inputBlock}><Input type="number" /><Button color="primary" className={styles.button}>SUBMIT</Button></div>
                        <div className={styles.contentBlock}><div dangerouslySetInnerHTML={{__html: data.Description}} /></div>
                    </div>
                    <ShortListingInfo data={data} />
                </div>
            </Container>
        );
    }
}

MakeBid.defaultProps = {
    data: {},
};

MakeBid.propTypes = {
    data: PropTypes.object,
};
