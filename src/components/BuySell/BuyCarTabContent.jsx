import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col,Container } from 'reactstrap';
import { SectionHeader, TabList, Tab, CardListHOC } from '../';
import { Icon, SearchBuySell, SimpleSearchForm } from '../';
import { cardTypes } from '../../constants';

const hero = './images/car-frame.png';

const titleImage = (<img src={hero} alt="" />);


export class BuyCarTabContent extends React.Component {
    constructor(props) {
        super(props);
        this.onSearchHandler = this.onSearchHandler.bind(this);
        this.state = {
            isSearch: false, // when state true, appears search result.Happens when you press the button "GO!"
        };
    }

    onSearchHandler() {
        this.setState({
            isSearch: true,
        });
    }

    render() {
        const { isSearch } = this.state;
        const { dictionaries, data, rData, cardType } = this.props;
        const searchBlock = (<SimpleSearchForm dropdowns={dictionaries} onSearchHandler={this.onSearchHandler} showSearchInput />);
        if (isSearch) {
            return (
                <div>
                    {searchBlock}
                    <Container>
                        <SearchBuySell data={data} cardType={cardType} />
                    </Container>
                </div>
            );
        }
        return (
            <div>
                {searchBlock}
                <SectionHeader icon={<Icon name='carfront' height={36} width={36} />} children='TOP CAR LISTINGS' hero={titleImage} cardType={cardType} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='carfront' height={36} width={36} />} children='TOP V8 LISTINGS' hero={titleImage} cardType={cardType} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={1} cardType={cardType}  />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='carfront' height={36} width={36} />} children='TOP DRIFT LISTINGS' hero={titleImage} cardType={cardType} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={2} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} currentPage={3} cardType={cardType} />
                    </Tab>
                </TabList>
            </div>
        );
    }
}

BuyCarTabContent.propTypes = {
    dictionaries: PropTypes.object,
    cardType: PropTypes.string.isRequired,
    data: PropTypes.object,
    rData: PropTypes.object,
};

BuyCarTabContent.defaultProps = {
    dictionaries: {},
    data: {},
    rData: {},
};

