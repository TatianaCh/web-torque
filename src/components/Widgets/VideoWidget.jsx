import React from 'react';
import styles from './VideoWidget.scss';
import { Modal, ModalBody } from 'reactstrap';
import PropTypes from 'prop-types';
import {Widget} from './Widget';

export const VideoWidget = ({preview, video, name, views, time, showBorder, onShow, ...props}) => {
    const videoComponent = (
        <a href={preview} onClick={onShow}>
            <img src={preview} alt="" />
        </a>
    );
    if (!showBorder) {
        return (
            <div>
                {videoComponent}
            </div>
        );
    }
    const viewsText = views === 1 ? '1 view' : `${views} views`;
    return (
        <div className={styles.videoWidget}>
            {videoComponent}
            <div className={styles.videoInfo}>
                <div className={styles.videoName}>{name}</div>
                <div className={styles.views}>
                    <span>{viewsText}</span>
                    <span className={styles.time}>{time}</span>
                </div>
            </div>
        </div>
    );
};

VideoWidget.defaultProps = {
    name: 'Video',
    time: '00:00',
    views: 0,
    showBorder: true,
}


VideoWidget.propTypes = {
    preview: PropTypes.string.isRequired,
    video: PropTypes.string,
    name: PropTypes.string,
    time: PropTypes.string,
    views: PropTypes.number,
    showBorder: PropTypes.bool,
    onShow: PropTypes.func,
    onHide: PropTypes.func,
};
