import React from 'react';
import styles from './ImageWidget.scss';
import { Modal, ModalBody } from 'reactstrap';
import PropTypes from 'prop-types';
import {Widget} from './Widget';


export const ImageWidget = ({img, thumbnailImg, alt, label, showBorder, showlabel, className2, onShow, ...props}) => {
    const containerClassName = className2 ? `${styles.ImageWidget} ${className2}` : styles.ImageWidget;
    const onClick = (e) => {
        e.preventDefault()
        if (typeof onShow == 'function') {
            onShow(img)
        }
    }
    const imageStyle = { 
        background: `url(${thumbnailImg}) center no-repeat`,
        backgroundSize: 'cover'
    };
    const image = (<a href={img} onClick={onClick} className={styles.img} style={imageStyle}></a>);
    if (!showlabel) {
        return (
            <Widget align="center" {...props} className={containerClassName}>
                {image}
            </Widget>
        );
    }

    if (!showBorder) {
        return image;
    }
    return (
        <Widget align="center" {...props} className={containerClassName}>
            {image}
            <div onClick={onClick} className={styles.label}>{label}</div>
        </Widget>
    );
};

ImageWidget.defaultProps = {
    label: 'View',
    showBorder: true,
    showlabel: true,
}


ImageWidget.propTypes = {
    img: PropTypes.string.isRequired,
    thumbnailImg: PropTypes.string.isRequired,
    alt: PropTypes.string,
    showBorder: PropTypes.bool,
    showlabel: PropTypes.bool,
    onShow: PropTypes.func,
    onHide: PropTypes.func,
};
