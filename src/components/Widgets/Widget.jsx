import React from 'react';
import { Col } from 'reactstrap';
import styles from './Widget.scss';
import classnames from 'classnames';
import PropTypes from 'prop-types';

export const Widget = ({align, className, ...props}) => (
    <Col {...props} className={`${className} ${styles.Widget} ${styles.Widget}_${align}`} />
)

Widget.defaultProps = {
    align: 'left'
};

Widget.propTypes = {
    align: PropTypes.oneOf(['center', 'left', 'right', 'justify']),
};
