import React from 'react';
import PropTypes from 'prop-types';
import { NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
import { pure } from 'recompose';
import styles from './TabLink.scss';


const PureTabLink = ({title, icon, onActivate, active, smallTab}) => {
    const tabClass = smallTab ? `${styles.TabLink} ${styles.smallTab}` : styles.TabLink ;
    return (
        <NavItem className={tabClass}>
            <NavLink
                className={classnames({ active: active, [styles.Active]: active })}
                onClick={ onActivate } >
                <div className={styles.Icon}>{ icon }</div>
                <div className={styles.title}> {title}</div>
            </NavLink>
        </NavItem>
    );
};

export const TabLink = pure(PureTabLink);
