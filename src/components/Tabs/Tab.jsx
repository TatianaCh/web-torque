import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import Fa from 'react-fontawesome';

/**
 * Represents a tab to display in a TabList
 */
export const Tab = ({children}) => children;

Tab.defaultProps = {
    title: "tab title",
    icon: <Fa name="star" />,
};

Tab.propTypes = {
    title: PropTypes.string,
    icon: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
}
