import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import { TabLink } from './TabLink';
import styles from './TabList.scss';
import { utils } from '../..';

const activeTab = withState('active', 'setActive', 0);

const TabListPure = ({children, active, setActive, onChangeTabCallback }) => (
    <div className={styles.TabList}>
        <Nav className={styles.Header} tabs>
            { children.map( (tab, i) => (
            <TabLink key={i}
                {...utils.string2Icon(tab.props)}
                smallTab={children.length > 4}
                active={active === i}
                onActivate={() => { setActive(i); if (onChangeTabCallback) { onChangeTabCallback(tab.key); }}} />
            ))}
        </Nav>

        <TabContent activeTab={active} className={styles.Content}>
            { children.map( (tab, i) => (
            <TabPane tabId={i} key={i}>
                { tab }
            </TabPane>
            )) }
        </TabContent>
    </div>
);

export const TabList = compose(
    activeTab,
    pure
)(TabListPure);
