import React from 'react';
import PropTypes from 'prop-types';
import { Container, Input, Collapse, Button, CardBody, Card  } from 'reactstrap';
import { cardTypes } from '../../constants';
import { isEmptyObject } from '../../utils';
import styles from './WriteReviewTabContent.scss';
import { WriteReviewForm, RatingValue, AccordionComponent, Icon, HelpButton } from '../';

export class WriteReviewTabContent extends React.Component {
    render() {
        const { categories } = this.props;
        const accordeonItems = Object.keys(categories).map((key) => {
            if (isEmptyObject(categories[key])) {
                return false;
            }
            const propertiesList = Object.keys(categories[key].values).map((valueLabel) => {
                return (
                    <div className={styles.propertyRating} key={`${valueLabel}_${key}`}>
                        <RatingValue value={categories[key].values[valueLabel]}/>
                        <h4 className={styles.subTitle}>{valueLabel}</h4>
                        <HelpButton description={valueLabel} iconClassName2={styles.categoryInfo} />
                    </div>
                );
            });

            return (
                <AccordionComponent className2={styles.accordionBlock} label={categories[key].label} key={key}>
                    <Card className={styles.card}>
                        <CardBody className={styles.cardBody}>
                            {propertiesList}
                        </CardBody>
                    </Card>
                </AccordionComponent>
            );
        })
        return (
            <Container className={styles.WriteReview}>
                <WriteReviewForm {...this.props} />
                <div className={styles.rating}>
                    <div className={styles.titleRating}>
                        <h4 className={styles.title}> RATINGS</h4>
                        <h4 className={styles.subTitle}>If you can`t rate a category leave it blank and it will be marked as not applicable</h4>
                    </div>
                    <div className={styles.overalBlock}>
                        <RatingValue value={3} widthIcons={47} heightIcons={47}/>
                        <div className={styles.overContent}>
                            <h3 className={styles.title}> OVERALL EMOTIONAL RATING</h3>
                            <h4 className={styles.subTitle}>Consisely describe this category to the reviewer so they can rate accurately</h4>
                        </div>
                    </div>
                    {accordeonItems}
                </div>
            </Container>
        );
    }
}

WriteReviewTabContent.defaultProps = {
    categories: {},
};

WriteReviewTabContent.propTypes = {
    categories: PropTypes.object,
};
