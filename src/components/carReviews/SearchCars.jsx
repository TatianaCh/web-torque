import React from 'react';
import PropTypes from 'prop-types';
import * as Rambda from 'rambda';
import { Container, Button } from 'reactstrap';
import { DropDown, CarSearchListHOC, FilterList } from '../';
import styles from './SearchCars.scss';


export class SearchCars extends React.Component {

    render() {
        const {
            data, cardType,
            filterKey, filterOptions, sortByOptions, sortBy, onSortChange, onSearchReset,
            totalCount, offset, currentPage, totalPage,
            onGoto, onNext, onBack, filterFormName} = this.props;

        return (
            <div className={styles.SearchCars}>
                <div className={styles.filter}>
                    <FilterList filterOptions={filterOptions} filterKey={filterKey} onSearchReset={onSearchReset} filterFormName={filterFormName} />
                </div>
                <div className={styles.list}>
                    <Container className={styles.titleBlock}>
                        <div>
                            <h2 className={styles.title}>SEARCH RESULTS</h2>
                            <h3 className={styles.subTitle}>
                                {data.items.length > 0 ?
                                    `SHOWING ${offset+1} TO ${ offset + data.items.length } OF ${totalCount} RESULT${ totalCount>1 ? "S" : "" }`:
                                    `No results found`
                                }.
                            </h3>
                        </div>
                        <div>
                            <h4 className={styles.soft}>SORT BY</h4>
                            <DropDown
                                className={styles.dropDownBy}
                                label="Sort By"
                                items={sortByOptions}
                                labelHidden
                                valueField="sortBy"
                                displayField="label"
                                value={sortBy}
                                onChange={onSortChange} />
                        </div>
                    </Container>
                    <CarSearchListHOC
                        cardType={cardType}
                        currentPage={currentPage}
                        totalPage={totalPage}
                        onGoto={onGoto}
                        onNext={onNext}
                        onBack={onBack}
                        data={data} lg={12} md={12} sm={12} xs={12} />
                </div>
            </div>
        );
    }
}

SearchCars.propTypes = {
    data: PropTypes.object,
    cardType: PropTypes.string,
    sortByOptions: PropTypes.array,
    sortBy: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onSortChange: PropTypes.func,
    onSearchReset: PropTypes.func,
};

SearchCars.defaultProps = {
    data: {},
    cardType: '',
    filterOptions: [],
    sortByOptions: []
};
