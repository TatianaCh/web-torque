import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col,Container  } from 'reactstrap';
import { SectionHeader, TabList, Tab, CardListHOC } from '../';
import { Icon, SearchCars, SimpleSearchForm } from '../';
import { cardTypes, sortByOptions } from '../../constants';

const hero = './images/cars.png';

const titleImage = (<img src={hero} alt="" />);
const cardType = cardTypes.carReviews;
import { CarFilters } from '../../../demo/mocks/CarFilters'




export class BrowseCarsTabContent extends React.Component {
    constructor(props) {
        super(props);
        this.onSearchHandler = this.onSearchHandler.bind(this);
        this.searchBlockData = {
            brands: { ...props.dictionaries.brands },
            models: { ...props.dictionaries.models },
            years: { ...props.dictionaries.years },
        };
        this.state = {
            isSearch: false,
        };
    }

    onSearchHandler() {
        this.setState({
            isSearch: true,
        });
    }

    render() {
        const { isSearch } = this.state;
        const { dictionaries, data, rData } = this.props;
        const searchBlock = (<SimpleSearchForm dropdowns={this.searchBlockData} onSearchHandler={this.onSearchHandler} />);
        if (isSearch) {
            return (
                <div>
                    {searchBlock}
                    <Container>
                        <SearchCars
                            {...dictionaries}
                            data={data}
                            cardType={cardType}
                            filterOptions={CarFilters}
                            sortByOptions={sortByOptions}
                            limit={4}
                            currentPage={8}
                            totalPage={10}
                            totalCount={37}
                            offset={32} />
                    </Container>
                </div>
            );
        }
        return (
            <div>
                {searchBlock}
                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children='TOP CAR REVIEWS' hero={titleImage} />
                <TabList>
                    <Tab title="TOP RATED" icon="speed">
                        <CardListHOC data={data} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon="glasses">
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'TOP V8 REVIEWS'} hero={titleImage} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} cardType={cardType} currentPage={1}  />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'TOP DRIFT REVIEWS'} hero={titleImage} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} cardType={cardType} currentPage={2} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} currentPage={3} />
                    </Tab>
                </TabList>
            </div>
        );
    }
}

BrowseCarsTabContent.propTypes = {
    dictionaries: PropTypes.object,
    data: PropTypes.object,
    rData: PropTypes.object,
};

BrowseCarsTabContent.defaultProps = {
    dictionaries: {},
    data: {},
    rData: {},
};
