import React from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'reactstrap';
import { cardTypes } from '../../constants';
import { isEmptyObject } from '../../utils';
import styles from './WriteReviewTabContent.scss';
import { DropDown } from '../';


export class WriteReviewForm extends React.Component {
    constructor(props) {
        super(props);
        this.dropdowns = {
            brands: props.dictionaries.brands,
            models: props.dictionaries.models,
            years: props.dictionaries.years,
        };
        if (!isEmptyObject(props.dictionaries.types)) {
            this.dropdowns.BodyShape = {
                label: 'BodyShape',
                placeholder: "Body Shape",
                options: props.dictionaries.types.options,
            };
        }
    }

    render() {
        const { title, showSocialLinksInputs } = this.props;
        const dropdownsList = Object.keys(this.dropdowns).map((key) => {
            if (isEmptyObject(this.dropdowns[key])) {
                return false;
            }
            return (
                <div className={styles.dropdownMargin} key={key}>
                    <DropDown
                        label={this.dropdowns[key].label}
                        name={key}
                        labelHidden
                        items={this.dropdowns[key].options}
                        placeholder={this.dropdowns[key].placeholder}
                    />
                </div>
            );
        });
        const socialLinkBlock = (
            <div className={styles.socialBlock}>
                <div className={styles.socialMargin}><Input className={styles.blockInput} type="text" name="facebook" placeholder="Link To Facebook" /></div>
                <div className={styles.socialMargin}><Input className={styles.blockInput} type="text" name="youtube" placeholder="Link To YouTube" /></div>
            </div>
        );
        return (
            <div className={styles.detail}>
                <div className={styles.titleBlock}>
                    <h2 className={styles.title}>{title}</h2>
                    <h4 className={styles.subTitle}>MANDATORY FIELDS</h4>
                </div>
                <div className={styles.dropdown}>
                    {dropdownsList}
                </div>
                {showSocialLinksInputs && socialLinkBlock}
                <div className={styles.reviewheadingBlock}>
                    <div className={styles.titleheadingBlock}>
                        <h2 className={styles.titleReview}> HEADLINE FOR YOUR REVIEW</h2>
                        <h4 className={styles.subtitleReview}>MAXIMUM 10 WORDS</h4>
                    </div>
                    <div className={styles.headingMargin}><Input className={styles.blockInput} type="text" name="heading" placeholder="" /></div>
                </div>
                <div className={styles.reviewWriteBlock}>
                    <div className={styles.titleheadingBlock}>
                        <h2 className={styles.titleReview}> WRITE YOUR REVIEW</h2>
                        <h4 className={styles.subtitleReview}>MAXIMUM 100 WORDS</h4>
                    </div>
                    <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                </div>
                <div className={styles.reviewWriteBlock}>
                    <div className={styles.titleheadingBlock}>
                        <h2 className={styles.titleReview}>SILLY HEADER</h2>
                        <h4 className={styles.subtitleReview}>MAXIMUM 100 WORDS</h4>
                    </div>
                    <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                </div>
                <div className={styles.reviewWriteBlock}>
                    <div className={styles.titleheadingBlock}>
                        <h2 className={styles.titleReview}>SILLY HEADER</h2>
                        <h4 className={styles.subtitleReview}>MAXIMUM 100 WORDS</h4>
                    </div>
                    <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                </div>
                <div className={styles.reviewWriteBlock}>
                    <div className={styles.titleheadingBlock}>
                        <h2 className={styles.titleReview}>SILLY HEADER</h2>
                        <h4 className={styles.subtitleReview}>MAXIMUM 100 WORDS</h4>
                    </div>
                    <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                </div>
                <div className={styles.buttonBlock}>
                    <Button color="blue" className={styles.addButton}>ADD IMAGES</Button>
                    <Button color="primary" className={styles.submitButton}>SUBMIT YOUR REVIEW</Button>
                </div>
            </div>
        );
    }
}

WriteReviewForm.defaultProps = {
    dictionaries: {
        brands: {},
        years: {},
        types: {}
    },
    title: '',
    showSocialLinksInputs: true,
};

WriteReviewForm.propTypes = {
    dictionaries: PropTypes.object,
    title: PropTypes.string,
    showSocialLinksInputs: PropTypes.bool,
};
