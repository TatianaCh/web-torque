import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { Input } from 'reactstrap';
import { LabelledField } from './LabelledField';
import style from './DropDown.scss';

export class DropDown extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            value: props.value,
        };
    }

    handleChange(value) {
        this.setState({
            value,
        });
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    render() {
        const { items, valueField, displayField, name, placeholder, ...args} = this.props;
        const { value } = this.state;
        const selectOptions = items.map(item => (
            { value: item[valueField], label: item[displayField] }
        ));
        const arrowRenderer = () => (false);

        const selectStyle = {
            'background': 'linear-gradient(-180deg, #969696 17%, #5E5E5E 100%)',
        }
        return (
            <LabelledField id={name} {...args}>
                <Select
                    className={style.dropDown}
                    options={selectOptions}
                    name={name}
                    id={name}
                    placeholder={placeholder}
                    arrowRenderer={arrowRenderer}
                    optionClassName={style.dropDownOption}
                    style={selectStyle}
                    value={value}
                    onChange={this.handleChange}
                    clearable={false}
                />
            </LabelledField>
        );
    }
};

DropDown.defaultProps = {
    valueField: 'ID',
    displayField: 'Name',
    placeholder: false,
    value: '',
    onChange: null,
}

DropDown.propTypes = {
    items: PropTypes.array,
    valueField: PropTypes.string,
    displayField: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func,
    placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.bool])
}
