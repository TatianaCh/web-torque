import React from 'react'
import PropTypes from 'prop-types'
import { RangedDropDown } from './RangedDropDown'
import { range } from 'lodash'

/**
 * Extension of the RangedDropDown that can be used to prepopulate the drop down
 * with a range of numeric values.
 */
export const NumericRangedDropDown = ({min, max, step, ...props}) => (
    <RangedDropDown
        items={range(min, max, step).map(val => ({num: val}))}
        valueField="num"
        displayField="num"
        {...props} />
)

NumericRangedDropDown.defaultProps = {
    min: 0,
    max: 0,
    step: 1
}

NumericRangedDropDown.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
}
