import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'reactstrap';
import { LabelledField } from './LabelledField';
import styles from './TextField.scss';
import { changeTimer } from '../../utils/changeTimer'

export const TextField = ({labelHidden, onChange, ...props }) => (
    <LabelledField className={styles.TextField} labelHidden={labelHidden} >
        <Input
            type="text"
            {...changeTimer(onChange)}
            {...props}  />
    </LabelledField>
)

TextField.defaultProps = {
    placeholder: false,
    labelHidden: false,
    onChange: null,
}

TextField.propTypes = {
    name: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func,
    placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    labelHidden: PropTypes.bool
}
