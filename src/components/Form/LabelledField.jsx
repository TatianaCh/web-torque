import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { FormGroup, Label, Input } from 'reactstrap';
import style from './DropDown.scss';


const PureLabelledField = ({label, labelHidden, id, children, className}) => {
    return (
        <FormGroup className={className}>
            <Label for={`#${id}`} hidden={labelHidden}>{label}</Label>
            { children }
        </FormGroup>
    );
};

PureLabelledField.defaultProps = {
    label: '',
    labelHidden: false,
    id: '',
    className: '',
}

export const LabelledField = compose(
    pure
)(PureLabelledField);
