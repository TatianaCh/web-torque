import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'reactstrap';
import { LabelledField } from './LabelledField';
import { DropDown } from './DropDown';
import styles from './RangedDropDown.scss';

const buildRange = (item, value, field) => Object.assign({}, value, {[field]: item.value})

export const RangedDropDown = ({placeholder, labelHidden, name, value, onChange, ...props }) => (
    <LabelledField className={styles.RangedDropDown} labelHidden={labelHidden} >
        <DropDown {...props}
            label="From"
            placeholder="From"
            labelHidden
            value={value.from}
            onChange={(item) => onChange(buildRange(item,value,'from'))}
            />
        <span className={styles.dropSpan}>TO</span>
        <DropDown {...props}
            label="To"
            placeholder="To"
            labelHidden
            value={value.to}
            onChange={(item) => onChange(buildRange(item,value,'to'))} />
    </LabelledField>
)

RangedDropDown.defaultProps = {
    placeholder: false,
    labelHidden: false,
    value: {},
    valueTo: '',
    onChange: () => {}
}

RangedDropDown.propTypes = {
    name: PropTypes.string,
    value: PropTypes.object,
    onChange: PropTypes.func,
    placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    labelHidden: PropTypes.bool,
    items: PropTypes.array.isRequired
}
