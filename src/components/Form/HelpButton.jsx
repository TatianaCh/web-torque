import React from 'react';
import PropTypes from 'prop-types';
import style from './HelpButton.scss';

export class HelpButton extends React.Component {
    render() {
        const { description, iconClassName2 } = this.props;
        const iconClass = iconClassName2 ? `${style.icon} ${iconClassName2}` : style.icon;
        return (
            <div className={style.helpBlock}>
                <div className={style.help}>
                    {description}
                </div>
                <span className={iconClass}>
                    ?
                </span>
            </div>
        );
    }
};

HelpButton.defaultProps = {
    description: '',
    iconClassName2: '',
}

HelpButton.propTypes = {
    description: PropTypes.string,
    iconClassName2: PropTypes.string,
}
