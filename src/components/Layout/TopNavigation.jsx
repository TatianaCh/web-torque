import React from 'react';
import PropTypes from 'prop-types';
import { Container, Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem, NavLink,
    ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Icon } from '../';
import { isEmptyObject } from '../../utils';
import styles from './Navigation.scss';

export class TopNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.onSigninClick = this.onSigninClick.bind(this);
        this.onSignOutClick = this.onSignOutClick.bind(this);
        this.onPopupToggle = this.onPopupToggle.bind(this);
        this.state = {
            isAuthenticated: false,
            isPopupOpened: false,
        };
    }

    onSigninClick() {
        this.setState({
            isAuthenticated: true,
        });
    }

    onSignOutClick() {
        this.setState({
            isAuthenticated: false,
        });
    }

    onPopupToggle() {
        this.setState({
            isPopupOpened: !this.state.isPopupOpened,
        });
    }

    generatePopup() {
        return (
            <DropdownMenu className={styles.profileDropdownMenu}>
                <div>
                    <DropdownItem><Link to="/dashboard">Dashboard</Link></DropdownItem>
                    <DropdownItem><Link to="/dashboard/watchlist">Watchlist</Link></DropdownItem>
                    <DropdownItem>Listings I won</DropdownItem>
                    <DropdownItem>Listings I lost</DropdownItem>
                    <DropdownItem>Your offers to sellers</DropdownItem>
                    <DropdownItem>Offers from sellers</DropdownItem>
                    <DropdownItem>Recently viewed</DropdownItem>
                </div>
                <div>
                    <DropdownItem>Rejected offers</DropdownItem>
                    <DropdownItem>List a car</DropdownItem>
                    <DropdownItem>List a part</DropdownItem>
                    <DropdownItem>Unsold items</DropdownItem>
                    <DropdownItem>My listings</DropdownItem>
                    <DropdownItem>My reviews</DropdownItem>
                    <DropdownItem>My profile</DropdownItem>
                    <DropdownItem onClick={this.onSignOutClick}>Log out</DropdownItem>
                </div>
            </DropdownMenu>
        );
    }

    render() {
        const { routes, useRouter, open, setOpen, profile } = this.props;
        const { isAuthenticated, isPopupOpened } = this.state;
        let navbar = (
            <Nav className="ml-auto" navbar>
                <NavItem onClick={this.onSigninClick}><div className="nav-link">Signin action</div></NavItem>
                {routes.map(( ({to, label, icon, icon_width}, i) => {
                    const width = icon_width ? icon_width : 19;
                    return (
                        <NavItem key={i}>
                            {useRouter ? (
                                <Link to={to} className="nav-link">
                                    {icon && <Icon color="blue" height={19} width={width} name={icon} /> }
                                    {label}
                                </Link>
                            ) : (
                                <NavLink href={to}>
                                    {icon && <Icon color="blue" height={19} width={width} name={icon} /> }
                                    {label}
                                </NavLink>
                            )}
                        </NavItem>
                    );
                }
                ))}
            </Nav>
        );
        if (isAuthenticated && !isEmptyObject(profile)) {
            const notificationsCounter = 12;
            const messagesCounter = 4;

            const navblockPopup = (
                <div>
                    <div className={styles.overlay} />
                    {this.generatePopup()}
                </div>
            )
            navbar = (
                <Nav className={`ml-auto ${styles.authenticatedNavbar}`} navbar>
                    <NavItem>
                        <ButtonDropdown className="nav-link" isOpen={isPopupOpened} toggle={this.onPopupToggle}>
                            <DropdownToggle caret className={styles.userNameContainer}>
                                <div className={styles.userAvatar}>
                                    <img src={profile.PreviewThumbnailURL} />
                                </div>
                                <span>Hi, {profile.Name}</span>
                            </DropdownToggle>
                            {this.generatePopup()}
                        </ButtonDropdown>
                    </NavItem>
                    <NavItem>
                        <div className={styles.alertIndicatorContainer}>
                            <Icon color="blue" height={19} name="notifications" />
                            {notificationsCounter > 0 && (<span className={styles.alertsCounter}>{notificationsCounter}</span>)}
                        </div>
                    </NavItem>
                    <NavItem>
                        <div className={styles.alertIndicatorContainer}>
                            <Icon color="blue" height={19} name="chat_bubble" />
                            {messagesCounter > 0 && (<span className={styles.alertsCounter}>{messagesCounter}</span>)}
                        </div>
                    </NavItem>
                    <NavItem><div className="nav-link">Search</div></NavItem>
                </Nav>
            );
        }
        return (
            <Container className={styles.SubNav}>
                <Collapse isOpen={open} navbar>
                    {navbar}
                </Collapse>
            </Container>
        );
    }
}

TopNavigation.defaultProps = {
    routes: [],
    useRouter: false,
    open: false,
    setOpen: null,
    profile: {},
};

TopNavigation.propTypes = {
    routes: PropTypes.array,
    useRouter: PropTypes.bool,
    open: PropTypes.bool,
    setOpen: PropTypes.func,
    profile: PropTypes.object,
};
