import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { ReviewItem } from '../';
import { List, SliderList } from '../Layout';


// Attach the data HoC to the pure component
export const ReviewsListHOC = compose(
    SliderList,
    List,
    pure,
)(ReviewItem);

ReviewsListHOC.defaultProps = {
    offset: 0,
    limit: 6,
    sortBy: [{field:'ID', direction:'ASC'}],
    hideViewAllBtn: true,
};

ReviewsListHOC.propTypes = {
    offset: PropTypes.number,
    limit: PropTypes.number,
    sortBy:  PropTypes.array,
    hideViewAllBtn: PropTypes.bool,
}
