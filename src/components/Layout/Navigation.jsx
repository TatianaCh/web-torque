import React from 'react';
import { withState, pure, compose } from 'recompose';
import { Row, Container, Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Icon, TopNavigation } from '../';
import styles from './Navigation.scss';

const withOpen = withState('open', 'setOpen', false);

const PureNav = ({routes, open, setOpen, useRouter, subRoutes, profile}) => (
    <Navbar expand="lg" dark className={styles.Navigation}>
        <TopNavigation routes={subRoutes} useRouter={useRouter} open={open} setOpen={setOpen} profile={profile} />
        <div className={styles.MainNavWrap}>
            <Container className={styles.MainNav}>
                <NavbarBrand href="/" className={styles.navBarBrand}>
                    <Icon name="logo_text" className2={styles.textIcon} />
                    <Icon name="logo_img" className2={styles.logoIcon} />
                </NavbarBrand>
                <NavbarToggler onClick={ _ => setOpen(!open) } />
                <Collapse isOpen={open} navbar>
                    <Nav className="ml-auto" navbar>
                        {routes.map(( ({to, label, icon, icon_width}, i) =>
                            <NavItem key={i}>
                                {useRouter ? (
                                    <Link to={to} className="nav-link">
                                        {icon && <Icon color="blue" height={19} width={icon_width} name={icon} /> }
                                        {label}
                                    </Link>
                                ) : (
                                    <NavLink href={to}>
                                        {icon && <Icon color="blue" height={19} width={icon_width} name={icon} /> }
                                        {label}
                                    </NavLink>
                                )}
                            </NavItem>
                        ))}
                    </Nav>
                </Collapse>
            </Container>
        </div>
    </Navbar>
);

PureNav.defaultProps = {
    useRouter: false,
}

export const Navigation = compose(
    withOpen,
    pure
)(PureNav);
