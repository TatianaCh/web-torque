import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { VideoWidget } from '../../components/Widgets';
import { List, SliderList } from '../Layout';

// Attach the data HoC to the pure component
export const VideosListHOC = compose(
    SliderList,
    List,
    pure,
)(VideoWidget);

VideosListHOC.defaultProps = {
    offset: 0,
    limit: 4,
    sortBy: [{field:'ID', direction:'ASC'}],
    hideViewAllBtn: true,
};

VideosListHOC.propTypes = {
    offset: PropTypes.number,
    limit: PropTypes.number,
    sortBy:  PropTypes.array
}
