import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { ImageWidget } from '../../components/Widgets';
import { List, SliderList } from '../Layout';

// Attach the data HoC to the pure component
export const ImagesListHOC = compose(
    SliderList,
    List,
    pure,
)(ImageWidget);

ImagesListHOC.defaultProps = {
    offset: 0,
    limit: 4,
    sortBy: [{field:'ID', direction:'ASC'}],
    hideViewAllBtn: true,
};

ImagesListHOC.propTypes = {
    offset: PropTypes.number,
    limit: PropTypes.number,
    sortBy:  PropTypes.array,
    hideViewAllBtn: PropTypes.bool,
}
