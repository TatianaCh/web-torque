import React from 'react';
import PropTypes from 'prop-types';
import { Container} from 'reactstrap';
import { NumberedPagination, Button } from '../';
import style from './Paginated.scss';

export const NumberedList = (ListComponent) => {
    const fn = ({maxPage, displayPage, currentPage, onNext, onBack, onGoto, totalPage, ...args}) => (
        <Container className={style.pagin}>
            {React.createElement(ListComponent, args)}
            <NumberedPagination
                totalPage={totalPage}
                currentPage={currentPage}
                displayPage={displayPage}
                onNext={onNext}
                onBack={onBack}
                onGoto={onGoto} />
        </Container>
    )

    return fn;
};
