import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { WorkshopReviewItem } from '../workshops';
import { List, SliderList } from '../Layout';

// Attach the data HoC to the pure component
export const WorkshopReviewsListHOC = compose(
    SliderList,
    List,
    pure,
)(WorkshopReviewItem);

WorkshopReviewsListHOC.defaultProps = {
    offset: 0,
    limit: 4,
    sortBy: [{field:'ID', direction:'ASC'}],
    showNumbers: true,
    hideViewAllBtn: true,
};

WorkshopReviewsListHOC.propTypes = {
    offset: PropTypes.number,
    limit: PropTypes.number,
    sortBy:  PropTypes.array,
    showNumbers: PropTypes.bool,
    hideViewAllBtn: PropTypes.bool,
}
