import React from 'react';
import PropTypes from 'prop-types';
import { SliderPagination } from '../';

/**
 * Display a paginated list with some slider pagination.
 * @param {Function} ListComponent Component that will display the list.
 */
export const PaginatedList = (ListComponent) => {
    const fn = ({maxPage, currentPage, onNext, onBack, onGoto, cycle, onViewAll, viewAllHref, viewAllLink, ...args}) => (
        <div>
            {React.createElement(ListComponent, args)}
            <SliderPagination
                maxPage={maxPage}
                currentPage={currentPage}
                onNext={onNext}
                onBack={onBack}
                onGoto={onGoto}
                cycle
                {...pageInfo}
                onViewAll={onViewAll}
                viewAllHref={viewAllHref}
                viewAllLink={viewAllLink}
                />
        </div>
    )

    return fn
};
