import React from 'react';
import { LoadingCard } from '../Loading/LoadingCard';
import { Container, Row, Col } from 'reactstrap';
import PropTypes from 'prop-types';
import * as Rambda from 'rambda';

/**
 * HOC that can render a list of DataObjects. Mostly used to render list of Card components.
 */
export const List = (childComponent) => {
    const fn = ({ data, loading, xs, sm, md, lg, dummyCount, cardType, limit, itemClassName, ...args }) => {
        const breakPoints = {
            xs: xs,
            sm: sm,
            md: md,
            lg: lg
        };

        if (loading) {
            return (
                <Row>
                    {Rambda.times( i => (
                        <Col {...breakPoints} key={i} className={itemClassName}><LoadingCard /></Col>
                    ), dummyCount)}
                </Row>
            );
        } else {
            let lastItemIndex = limit || data.items.length;
            if (lastItemIndex > data.items.length) {
                lastItemIndex = data.items.length;
            }
            const dataItems = [];
            for (let i = 0; i < lastItemIndex; i++) {
                const cardData = {
                    cardType,
                    ...data.items[i],
                    ...args
                };
                dataItems.push(
                    <Col key={i} {...breakPoints} className={itemClassName}>
                        {React.createElement(childComponent, cardData)}
                    </Col>
                );
            }
            return (
                <Row className='items-list'>
                    {dataItems}
                </Row>
            );
        }
    };

    fn.defaultProps = {
        xs: 12,
        sm: 6,
        md: 4,
        lg: 3,
        dummyCount: 4,
        limit: 0,
        itemClassName: '',
    };

    fn.propTypes = {
        xs: PropTypes.number,
        sm: PropTypes.number,
        md: PropTypes.number,
        lg: PropTypes.number,
        dummyCount: PropTypes.number,
        limit: PropTypes.number,
        itemClassName: PropTypes.string,
    }

    return fn;
}
