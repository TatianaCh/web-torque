import React from 'react';
import PropTypes from 'prop-types';
import { Container} from 'reactstrap';
import { SliderPagination, Button } from '../';
import style from './Paginated.scss';

export const SliderList = (ListComponent) => {
    const fn = ({maxPage, currentPage, onNext, onBack, onGoto, onViewAll, viewAllHref, viewAllLink, cycle, showNumbers, hideViewAllBtn, ...args}) => (
        <Container className={style.pagin}>
            {React.createElement(ListComponent, args)}
            <SliderPagination
                maxPage={maxPage}
                currentPage={currentPage}
                onNext={onNext}
                onBack={onBack}
                onGoto={onGoto}
                cycle={cycle}
                showNumbers={showNumbers} />
            {!hideViewAllBtn && <Button
                color="secondary"
                className={style.button}
                onClick={onViewAll}
                href={viewAllHref}
                link={viewAllLink}>VIEW ALL</Button>}
        </Container>
    )

    return fn;
};
