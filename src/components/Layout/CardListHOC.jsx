import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { CardListItem } from '../';
import { List, SliderList } from '../Layout';


// Attach the data HoC to the pure component
export const CardListHOC = compose(
    SliderList,
    List,
    pure,
)(CardListItem);

CardListHOC.defaultProps = {
    offset: 0,
    limit: 4,
    sortBy: [{field:'ID', direction:'ASC'}]
};

CardListHOC.propTypes = {
    offset: PropTypes.number,
    limit: PropTypes.number,
    sortBy:  PropTypes.array
}
