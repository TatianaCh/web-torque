import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { HorizontalCard } from '../';
import { List, NumberedList } from '../Layout';


// Attach the data HoC to the pure component
export const CarSearchListHOC = compose(
    NumberedList,
    List,
    pure,
)(HorizontalCard);

CarSearchListHOC.defaultProps = {
    offset: 0,
    limit: 6,
    sortBy: [{field:'ID', direction:'ASC'}],
    showNumbers: true,
    hideViewAllBtn: true,
};

CarSearchListHOC.propTypes = {
    offset: PropTypes.number,
    limit: PropTypes.number,
    sortBy: PropTypes.array,
    showNumbers: PropTypes.bool,
    hideViewAllBtn: PropTypes.bool,
}
