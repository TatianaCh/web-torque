import React from 'react';
import propTypes from 'prop-types';
import HomeNavigationItem from './HomeNavigationItem';
import s from './HomeNavigation.scss';

export const HomeNavigation = ({ routes, useRouter }) => {
    const routesList = [];
    routes.forEach(item => {
        const text = item.homepage_label || item.label;
        if (item.to.indexOf('home') > 0) {
            return;
        }
        let className = s.left;
        if (item.to.indexOf('workshop') > 0) {
            className = s.left;
        } else if (item.to.indexOf('buysell') > 0) {
            className = s.leftBottom;
        } else if (item.to.indexOf('events') > 0) {
            className = s.bottom;
        } else if (item.to.indexOf('reviews') > 0) {
            className = s.rightBottom;
        } else if (item.to.indexOf('drivers') > 0) {
            className = s.right;
        }
        routesList.push(
            <HomeNavigationItem key={item.to} text={text} url={item.to} icon={item.icon} iconWidth={item.icon_width} className2={className} useRouter={useRouter} />
        );
    });
    return (
        <div className={s.homeNavBlock}>
            {routesList}
        </div>
    );
};

HomeNavigation.defaultProps = {
    routes: [],
    useRouter: false,
};

HomeNavigation.propTypes = {
    routes: propTypes.array,
    useRouter: propTypes.bool,
};
