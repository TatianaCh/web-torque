import React from 'react';
import propTypes from 'prop-types';
import { NavLink  } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Icon } from '../';
import styles from './HomeNavigation.scss'

const HomeNavigationItem = ({ url, text, className2, icon, iconWidth, useRouter }) => {
    const itemClassName = `${styles.navItem} ${className2}`;
    const width = iconWidth ? iconWidth * 2.2 : 40;
    if (useRouter) {
        return (
            <Link to={url} className={itemClassName}>
                <div className={styles.text}>
                    {text}
                    <div className={styles.arrow} />
                </div>
                {icon && <Icon color="blue" height={32} width={width} name={icon} /> }
            </Link>
        );
    }
    return (
        <NavLink href={url} className={itemClassName}>
            <div className={styles.text}>
                {text}
                <div className={styles.arrow} />
            </div>
            {icon && <Icon color="blue" height={32} width={width} name={icon} /> }
        </NavLink>
    );
};

HomeNavigationItem.defaultProps = {
    text: '',
    icon: '',
    className2: '',
    useRouter: false,
};

HomeNavigationItem.propTypes = {
    url: propTypes.string.isRequired,
    text: propTypes.string,
    icon: propTypes.string,
    className2: propTypes.string,
    useRouter: propTypes.bool,
};


export default HomeNavigationItem;
