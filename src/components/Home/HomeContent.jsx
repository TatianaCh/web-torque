import React from 'react';
import { withState, pure, compose } from 'recompose';
import { Icon } from '../';
import { Button } from 'reactstrap';
import styles from './HomeContent.scss';


const PureHomeContent = () => (
    <div className={styles.root}>
        <div className={styles.HomeContent}>
            <div className={styles.main}>
                <div className={styles.images}>
                    <div className={styles.icon}>
                        <Icon color="blue" height={50} width={50} name='car' />
                    </div>
                    <div className={styles.img}>
                        <img src="./images/render_Tire.png" alt=""/>
                    </div>
                </div>
                <div className={styles.info}>
                    <span>Cta title</span>
                    <p>Lorem ipsum dolor sit amet, elit  adipiscing.
                        Suspendisse eget lorem quis.
                    </p>
                    <div className={styles.view}>
                        <Button color="primary" tag="a" href='#'>VIEW</Button>
                    </div>
                </div>
            </div>
            <div className={styles.main}>
                <div className={styles.images}>
                    <div className={styles.icon}>
                        <Icon color="blue" height={32} width={32} name='double_wrench' />
                    </div>
                    <div className={styles.img}>
                        <img src="./images/render_Tools.png" alt=""/>
                    </div>
                </div>
                <div className={styles.info}>
                    <span>Cta title</span>
                    <p>Lorem ipsum dolor sit amet, elit  adipiscing.
                        Suspendisse eget lorem quis.
                    </p>
                    <div className={styles.view}>
                        <Button color="primary" tag="a" href='#'>VIEW</Button>
                    </div>
                </div>
            </div>
            <div className={styles.main}>
                <div className={styles.images}>
                    <div className={styles.icon}>
                        <Icon color="blue" height={32} width={32} name='calendar' />
                    </div>
                    <div className={styles.img}>
                        <img src="./images/computer.png" alt=""/>
                    </div>
                </div>
                <div className={styles.info}>
                    <span>Cta title</span>
                    <p>Lorem ipsum dolor sit amet, elit  adipiscing.
                        Suspendisse eget lorem quis.
                    </p>
                    <div className={styles.view}>
                        <Button color="primary" tag="a" href='#'>VIEW</Button>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export const HomeContent = compose(
    pure
)(PureHomeContent);
