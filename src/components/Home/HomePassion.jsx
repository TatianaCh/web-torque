import React from 'react';
import { withState, pure, compose } from 'recompose';
import { Icon } from '../';
import { Button, CardImg, Container } from 'reactstrap';
import styles from './HomePassion.scss';


const PureHomePassion = () => (
    <div className={styles.HomePassion}>
        <Container className={styles.section}>
            <div className={styles.info}>
                <h2 className={styles.title}>PASSION FOR CARS.</h2>
                <p className={styles.subInfo}>
                    Lorem ipsum dolor sit amet, conse ctetur adipiscing elit.
                    Aliquam congu nibh nec dignissim. Lorem ipsum dolor sit amet,
                    conse ctetur adipiscing elit. Aliquam congu nibh nec dignissim.
                    Lorem ipsum dolor sit amet, conse ctetur adipiscing elit.
                    Aliquam congu nibh nec dignissim
                </p>
                <div className={styles.readMore}>
                    <Button color="secondary" tag="a" href='#'>READ MORE</Button>
                </div>
            </div>
            <div className={styles.card}>
                <div className={styles.line} />
                <div className={styles.cardContent}>
                    <div className={styles.wrapper}>
                        <div className={styles.ImageWrapper}>
                            <div className={styles.image}>
                                <CardImg top width="100%" src="/images/cta3.png" />
                            </div>
                        </div>
                    </div>
                    <div className={styles.wrapper}>
                        <div className={styles.ImageWrapper}>
                            <div className={styles.image}>
                                <CardImg top width="100%" src="/images/cta2.png" />
                            </div>
                        </div>
                    </div>
                    <div className={styles.wrapper}>
                        <div className={styles.ImageWrapper}>
                            <div className={styles.image}>
                                <CardImg top width="100%" src="/images/cta1.png" />
                            </div>
                        </div>
                    </div>
                    <div className={styles.wrapper}>
                        <div className={styles.ImageWrapper}>
                            <div className={styles.image}>
                                <CardImg top width="100%" src="/images/cta.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    </div>

);

export const HomePassion = compose(
    pure
)(PureHomePassion);
