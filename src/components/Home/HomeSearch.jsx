import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { Button, Input } from 'reactstrap';
import { Icon } from '../';
import styles from './HomeSearch.scss';


export class HomeSearch extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            value: props.value,
        };
    }

    handleChange(value) {
        this.setState({
            value,
        });
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }
    render() {
        const { categories, valueField, displayField } = this.props;
        const { value } = this.state;
        const selectStyle = {
            'background': 'transparent',
        };
        const selectOptions = categories.map(item => (
            { value: item[valueField], label: item[displayField] }
        ));
        const arrowRenderer = () => (
            <div className={styles.selectArrow} />
        );
        return (
            <div className={styles.HomeSearch}>
                <div className={styles.homeNavContent}>
                    <Input className={styles.search} type="search" name="search" placeholder="Search" />
                    <Select
                        className={styles.dropDown}
                        options={selectOptions}
                        placeholder='ALL CATEGORIES'
                        arrowRenderer={arrowRenderer}
                        optionClassName={styles.dropDownOption}
                        style={selectStyle}
                        value={value}
                        onChange={this.handleChange}
                        clearable={false}
                    />
                    <Button className={styles.searchButton}>Go!</Button>
                </div>
            </div>
        );
    }
};

HomeSearch.propTypes = {
    categories: PropTypes.array,
    valueField: PropTypes.string,
    displayField: PropTypes.string,
};

HomeSearch.defaultProps = {
    categories: [],
    valueField: 'ID',
    displayField: 'Name',
}
