import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button as BootstrapButton } from 'reactstrap';

/**
 * Generic wrapper around the reactstrap button that can switch between:
 * * a form button with an onClick action
 * * a router link with a link attribute
 * * a standard html anchor with an href attribute
 */
export const Button = ({ onClick, href, link, ...props }) => {
    let extraProps;

    if (onClick) {
        extraProps = { onClick };
    } else if (link) {
        const { children } = props;
        return (
            <BootstrapButton {...props}>
                <Link to={link}>{children}</Link>
            </BootstrapButton>
        );
    } else {
        extraProps = {
            tag: "a",
            href
        }
    }

    return (<BootstrapButton {...props} {...extraProps} />)
}
