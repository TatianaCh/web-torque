import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col,Container } from 'reactstrap';
import { SectionHeader, TabList, Tab, CardListHOC } from '../';
import { Icon, SearchDrivers, SimpleSearchForm } from '../';
import { cardTypes } from '../../constants';

const hero = './images/cars.png';

const titleImage = (<img src={hero} alt="" />);



export class DriversLandingTabContent extends React.Component {
    constructor(props) {
        super(props);
        this.onSearchHandler = this.onSearchHandler.bind(this);
        this.searchBlockData = {
            types: { ...props.dictionaries.types },
            places: { ...props.dictionaries.places },
            years: { ...props.dictionaries.years },
        };
        this.state = {
            isSearch: false,
        };
    }

    onSearchHandler() {
        this.setState({
            isSearch: true,
        });
    }

    render() {
        const { isSearch } = this.state;
        const { dictionaries, data, rData, cardType } = this.props;
        const searchBlock = (<SimpleSearchForm dropdowns={this.searchBlockData} onSearchHandler={this.onSearchHandler} showSearchInput />);
        if (isSearch) {
            return (
                <div>
                    {searchBlock}
                    <Container>
                        <SearchDrivers
                            {...dictionaries}
                            data={data}
                            cardType={cardType}
                            limit={4}
                            currentPage={8}
                            totalPage={10}
                            totalCount={37}
                            offset={32} />
                        />
                    </Container>
                </div>
            );
        }
        return (
            <div>
                {searchBlock}

                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children='DRIVER TYPE 1' />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'DRIVER TYPE 2'} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={1} cardType={cardType}  />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'DRIVER TYPE 3'} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={2} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData}  currentPage={3} cardType={cardType} />
                    </Tab>
                </TabList>

                <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'DRIVER TYPE 4'} />
                <TabList>
                    <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
                        <CardListHOC data={data} currentPage={2} cardType={cardType} />
                    </Tab>
                    <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
                        <CardListHOC data={rData}  currentPage={3} cardType={cardType} />
                    </Tab>
                </TabList>
            </div>
        );
    }
}

DriversLandingTabContent.propTypes = {
    dictionaries: PropTypes.object,
    cardType: PropTypes.string.isRequired,
    data: PropTypes.object,
    rData: PropTypes.object,
};

DriversLandingTabContent.defaultProps = {
    dictionaries: {},
    data: {},
    rData: {},
};
