import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import styles from './AboutDriver.scss';

export class AboutDriver extends React.Component {
    render() {
        const { AttributionUrl, Description, Sponsors } = this.props.data;
        const sponsors = Sponsors.map(item => (
            <a className={styles.sponsorLogo} key={item.id} title={item.name} href={item.url}>
                <img src={item.logo} alt={item.name}/>
            </a>
        ));
        return (
            <Row className={styles.AboutBlock}>
                <Col xs={12} lg={7}>
                    <div className={styles.contentBlock}>
                        <div dangerouslySetInnerHTML={{__html: Description}} />
                    </div>
                </Col>
                 <Col xs={12} lg={5}>
                    <div className={styles.imagesBlock}>
                        <h1 className={styles.title}>driver sponsors</h1>
                        {sponsors}
                        <h2 className={styles.subTitle}>become a sponsor</h2>

                        <div className={styles.note}>
                            <p>We are still looking for sponsors.</p>
                            <p>Email <a href={AttributionUrl}>john@trackday.co.nz</a> if you are interested.</p>
                        </div>
                    </div>
                </Col>
            </Row>
        );
    }
}

AboutDriver.defaultProps = {
    data: {},
};

AboutDriver.propTypes = {
    data: PropTypes.object,
};
