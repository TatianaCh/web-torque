import React from 'react';
import PropTypes from 'prop-types';
import { Container, Button } from 'reactstrap';
import { DropDown, AccordionComponent, CarSearchListHOC } from '../';
import { sortByOptions } from '../../constants';
import styles from './SearchDrivers.scss';


export class SearchDrivers extends React.Component {
    render() {
        const { data, cardType, offset, totalCount } = this.props;

        let countText = 'No results found';
        if (data.items.length) {
            countText = `SHOWING ${offset + 1} TO ${offset + data.items.length} OF ${totalCount} RESULT${totalCount > 1 ? "S" : ""}`;
        }

        return (
            <div className={styles.SearchCars}>
                <div className={styles.filter}>
                    <div className={styles.titleBlock}>
                        <h2 className={styles.title}>REFINE SEARCH</h2>
                        <span className={styles.reset}>RESET</span>
                    </div>
                    <div className={styles.titleBlock}>
                        <h4 className={styles.subTitle}>Select the rating parameters for each category</h4>
                    </div>
                    <div className={styles.buttonBlock}>
                        <Button color="blue" className={styles.addButton}>ADVANCED FILTERS</Button>
                    </div>
                    <div>
                        <AccordionComponent className2={styles.filterAccordion} label="specialty">
                            <div>Hi</div>
                        </AccordionComponent>
                    </div>
                </div>
                <div className={styles.list}>
                    <Container className={styles.titleBlock}>
                        <div>
                            <h2 className={styles.title}>SEARCH RESULTS</h2>
                            <h3 className={styles.subTitle}>{countText}</h3>
                        </div>
                        <div>
                            <h4 className={styles.soft}>SORT BY</h4>
                            <DropDown
                                className={styles.dropDownBy}
                                label=""
                                items={sortByOptions}
                                placeholder="Drop Down Title"
                                labelHidden
                            />
                        </div>
                    </Container>
                    <CarSearchListHOC  cardType={cardType}  data={{ items: data.items }} lg={12} md={12} sm={12} xs={12} />
                </div>
            </div>
        );
    }
}

SearchDrivers.propTypes = {
    cardType: PropTypes.string.isRequired,
    data: PropTypes.object,
};

SearchDrivers.defaultProps = {
    data: {},
};
