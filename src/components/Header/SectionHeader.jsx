import React from 'react';
import styles from './SectionHeader.scss';
import {Icon} from '../';

export class SectionHeader extends React.Component {
    render() {
        const props = this.props;
        const secondLine = props.secondLine ? (<div className={styles.Title}>{ props.secondLine }</div>) : false;
        const iconBlock = (
            <div className={styles.Icon}>
                <div className={styles.icons}>
                { typeof props.icon === 'string' ?
                    <Icon name={props.icon} color="blue" height={32} width={32} /> :
                    props.icon
                }
                </div>
            </div>
        );
        const headerClassName = props.className2 ? `${styles.SectionHeader} ${props.className2}` : styles.SectionHeader;
        return (
            <section className={headerClassName}>
                {props.icon && iconBlock}
                <div className={styles.titleContainer}>
                    <div className={styles.Title}>{ props.children }</div>
                    {secondLine}
                    <div className={styles.Hero}>
                        { typeof props.hero === 'string' ?
                            <img src={props.hero} alt="" /> :
                            props.hero
                        }
                    </div>
                </div>
            </section>
        );
    }
}

SectionHeader.defaultProps = {
    icon: false,
    className2: '',
};
