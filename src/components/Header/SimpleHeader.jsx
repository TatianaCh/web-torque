import React from 'react';
import { withState, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import styles from './SimpleHeader.scss';

const PureSimpleHeader = ({children}) => (
    <h1 className={styles.SimpleHeader}>{ children }</h1>
);


/**
 * Most simple header available on the site.
 */
export const SimpleHeader = compose(
    pure
)(PureSimpleHeader);
