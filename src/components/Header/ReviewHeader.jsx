import React from 'react';
import { Link } from 'react-router-dom';
import { pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'reactstrap';
import { SimpleHeader } from './SimpleHeader';
import { OverallRating } from '../';
import styles from './ReviewHeader.scss';

const PureReviewHeader = ({actionLabel, onAction, actionUrl, rating, reviewCount, children}) => (
    <div className={styles.ReviewHeader}>
        <div className={styles.titleBlock}>
            <SimpleHeader>{ children }</SimpleHeader>
        </div>
        <div className={styles.ratingBlock}>
            <Row>
                <Col xs={12} md={7} lg={4}><OverallRating value={rating} reviewCount={reviewCount} /></Col>
                <Col xs={12}  md={5} lg={4}>
                    <Button className={styles.button} color="primary" onClick={onAction}>
                    <Link to={actionUrl}>{actionLabel}</Link>
                    </Button>
                </Col>
            </Row>
        </div>
    </div>
);

/**
 * Most simple header available on the site.
 */
export const ReviewHeader = compose(
    pure
)(PureReviewHeader);

ReviewHeader.propTypes = {
    actionLabel: PropTypes.string,
    onAction: PropTypes.func,
    actionUrl: PropTypes.string,
    rating: PropTypes.number,
    reviewCount: PropTypes.number,
}
