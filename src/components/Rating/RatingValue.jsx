import React from 'react';
import { Button, Label, FormGroup, Input } from 'reactstrap';
import PropTypes from 'prop-types';
import { Icon } from '../Icon/Icon';

export class RatingValue extends React.Component {

    starList(classes) {
        const { outOf, value, icon, editable, widthIcons, heightIcons } = this.props;
        const stars = [];
        const onChange =
            (i) => (() => this.props.onChange && editable && this.props.onChange(i));
        const imageProps = {
            width: widthIcons,
            height: heightIcons,
            name: icon,
        };
        for(let i = 1; i <= outOf; i++) {

            let color = '';
            if (i <= value) {
                color = 'blue';
            }

            stars.push((
                <li key={`star${i}`} className="star" onClick={onChange(i)}>
                    <Icon {...imageProps} color={color} />
                </li>
            ));
        }

        return stars;
    }

    render() {
        const width = {width: `${(this.props.value / this.props.outOf) * 100}%`};
        return (
            <ul className="rating-list">
                {this.starList()}
            </ul>
        );
    }
}

RatingValue.defaultProps = {
    value: 0,
    widthIcons: 23,
    heightIcons: 16,
    outOf: 5,
    editable: false,
    icon: 'speed',
};

RatingValue.propTypes = {
    value: PropTypes.number,
    outOf: PropTypes.number,
    widthIcons: PropTypes.number,
    heightIcons: PropTypes.number,
    editable: PropTypes.bool,
    icon: PropTypes.string, // we will accept only svg image name
    onChange: PropTypes.func,
};
