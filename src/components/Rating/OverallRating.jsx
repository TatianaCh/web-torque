import React from 'react';
import { Row, Col } from 'reactstrap';
import { RatingValue } from './RatingValue';
import PropTypes from 'prop-types';
import styles from './OverallRating.scss';

/**
 * Display an Overall rating for an object
 */
export const OverallRating = ({reviewCount, value, ...props}) => (
    <Row className={styles.OverallRating}>
        <Col xs={4} md={3}>
            <div className={styles.value}>{value.toFixed(1)}</div>
            <div className={styles.subTitle}>OVERALL</div>
        </Col>
        <Col xs={8} md={9} className={styles.rating}>
            <RatingValue widthIcons={30} heightIcons={30} value={value} {...props} />
            {reviewCount > 0 && (
                <div className={styles.review}>{reviewCount} reviews</div>
            )}
        </Col>
    </Row>
)

OverallRating.defaultProps = {
    value: 0,
    reviewCount: 0,
};

OverallRating.propTypes = {
    value: PropTypes.number,
    reviewCount: PropTypes.number,
}
