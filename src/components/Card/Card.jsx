import React from 'react';
import { pure, compose } from 'recompose';
import { Card as BtCard, CardBody, CardTitle, CardSubtitle, Button, Row, Col } from 'reactstrap';
import PropTypes from 'prop-types';
import { cutText } from '../../utils';
import styles from './Card.scss';
import { CardImage } from './CardImage';

export const PureCard = ({ title, subTitle, img, url, children, alt, info }) => (
    <BtCard className={styles.Card}>
        <CardBody className={styles.cardHeader}>
            <CardTitle className={styles.Title}>{title}</CardTitle>
            {subTitle && <CardSubtitle className={styles.SubTitle}>{subTitle}</CardSubtitle>}
        </CardBody>
        <CardImage
            src={ img }
            link={ url }
            alt={alt}>{info}</CardImage>
        <CardBody className={styles.info}>
            { children }
        </CardBody>
    </BtCard>
);

// Attach the data HoC to the pure component
export const Card = compose(
  pure,
)(PureCard);


Card.defaultProps = {
};

Card.propTypes = {
    title: PropTypes.string,
    subTitle: PropTypes.string,
    img: PropTypes.string,
    url: PropTypes.string,
    alt: PropTypes.string,
    info: PropTypes.node,
}
