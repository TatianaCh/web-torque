import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Card as BtCard, CardBody, CardText, Row, Col, Button } from 'reactstrap';
import { FormatDate, cutText } from '../../utils';
import { cardTypes } from '../../constants';
import { Card } from './Card';
import { RatingValue } from '../';
import styles from './CardListItem.scss';

export class CardListItem extends React.Component {
    info() {
        const { cardType, InfoImg, ReviewAverage } = this.props;
        switch (cardType) {
            case cardTypes.events:
            case cardTypes.buySell:
                return (
                    <h3 className={styles.subRating}>{InfoImg}</h3>
                );
            case cardTypes.driver:
                return false;
            default:
                return (
                   <RatingValue value={ReviewAverage} />
                );
        }
    }

    render() {
        const props = this.props;
        if (props.cardType === cardTypes.viewAllLink) {
            return (
                <BtCard className={styles.viewAllLink}>
                    <CardBody className={styles.Body}>
                        <CardText className={styles.viewAllLabel}>{props.label}</CardText>
                        <Button color="primary" className={styles.Button}>
                            <Link to={props.Url}>Go!</Link>
                        </Button>
                    </CardBody>
                </BtCard>
            );
        }
        let region = false;
        let bottomBlock = (
            <Row className={styles.cardInfo}>
                <Col xs={6}>{FormatDate(props.Created)}</Col>
                <Col xs={6}>{props.Views} Views</Col>
            </Row>
        );
        const cardProps = {
            title: props.Title,
            img: props.PreviewThumbnailURL,
            url: props.Url,
            alt: props.Title,
            info: this.info(),
        }

        if (props.cardType === cardTypes.buySell) {
            region = (<CardText className={styles.region}>{props.Region}</CardText>);
            bottomBlock = (
                <Row className={styles.cardInformation}>
                    <Col xs={12} xl={7} lg={12}><Button className={styles.button}>
                        <span className={styles.icon}>+</span>
                        <p className={styles.add}>ADD TO WATCHLIST</p></Button></Col>
                    <Col xs={12} xl={5} lg={12}>{props.Views} Views</Col>
                </Row>
            );
            cardProps.subTitle = props.SubTitle;
        }
        return (
            <Card {...cardProps}>
                <CardText className={styles.summary}>{cutText(props.Summary, 100)}</CardText>
                {region}
                {bottomBlock}
            </Card>
        );
    }
}
CardListItem.propTypes = {
    title: PropTypes.string,
    img: PropTypes.string,
    url: PropTypes.string,
    alt: PropTypes.string,
    info: PropTypes.node,
    Summary: PropTypes.string,
    Created: PropTypes.string,
    Views: PropTypes.number,
    cardType: PropTypes.string,
}
