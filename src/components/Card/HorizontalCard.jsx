import React from 'react';
import PropTypes from 'prop-types';
import { Card as BtCard, CardBody, CardTitle, CardText } from 'reactstrap';
import { FormatDate, cutText } from '../../utils';
import { cardTypes } from '../../constants';
import { RatingValue, Button } from '../';
import styles from './HorizontalCard.scss';

export class HorizontalCard extends React.Component {
    render() {
        const props = this.props;
        const isDriver = props.cardType === cardTypes.driver ;
        const isbuySell = props.cardType === cardTypes.buySell ;
        const infoClass = isDriver ? `${styles.info} ${styles.infoDriver}` : styles.info;
        const buttonText = isDriver || isbuySell ? 'view listing' : 'view';
        const dateText = isDriver || isbuySell ? `Closes ${FormatDate(props.Created)}` : FormatDate(props.Created);
        const addToWatchList = (
            <Button className={`${styles.button} ${styles.addButton}`}>
                <span className={styles.icon}>+</span>
                <p className={styles.add}>ADD TO WATCHLIST</p>
            </Button>
        );
        const ratingBlock = (
            <div className={styles.rating}>
                <RatingValue value={props.ReviewAverage} />
            </div>
        );
        const priceBlock = (
            <div className={styles.price}>
                {props.InfoImg}
            </div>
        );

        const imageStyle = { 
            backgroundImage: `url('${props.PreviewThumbnailURL}')`,
        };
        const showRatingBlock = props.cardType !== cardTypes.driver && props.cardType  !== cardTypes.buySell;
        return (
            <BtCard className={styles.Card}>
                <div className={styles.ImageWrapper}>
                    <div className={styles.image} style={imageStyle} />
                </div>
                <CardBody className={infoClass}>
                    <CardTitle className={styles.title}>{props.Title}</CardTitle>
                    {isbuySell && priceBlock}
                    {showRatingBlock  && ratingBlock}
                    <CardText className={styles.summary}>{props.Summary}</CardText>
                    <div className={styles.cardInfo}>
                        <span className={styles.date}>{dateText}</span>
                        {(isDriver || isbuySell)  && addToWatchList}
                        <span className={styles.views}>{props.Views} Views</span>
                    </div>
                    <div className={styles.ButtonWrapper}>
                        <Button color="primary" className={styles.Button} link={props.Url}>{buttonText}</Button>
                    </div>
                </CardBody>
            </BtCard>
        );
    }
}

HorizontalCard.defaultProps = {
};

HorizontalCard.propTypes = {
    Title: PropTypes.string,
    ID: PropTypes.string,
    PreviewThumbnailURL: PropTypes.string,
    InfoImg: PropTypes.string,
    ReviewAverage: PropTypes.number,
    Url: PropTypes.string,
    Summary: PropTypes.string,
    Views: PropTypes.number,
    Created: PropTypes.string,
}
