import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { pure, compose } from 'recompose';
import { CardImg, Button, Row, Col } from 'reactstrap';
import { RatingValue } from '../Rating';
import styles from './CardImage.scss';


export const PureCardImage = ({src, link, button, children, ...cardImgArgs}) => {
    const imageStyle = { 
        background: `url(${src}) center no-repeat`,
        backgroundSize: 'cover'
    };
    return (
        <div className={styles.CardImage}>
            <div className={styles.ImageWrapper}>
                <div className={styles.image} style={imageStyle}>
                </div>
            </div>
            <Row>
                <Col xs={7}>
                    <Col xs={12} className={styles.rating}>
                        { children }
                    </Col>
                </Col>
                <Col xs={5}>
                    <div className={styles.ButtonWrapper}>
                        <Button color="primary" className={styles.Button} tag={Link} to={link}>
                            {button}
                        </Button>
                    </div>
                </Col>
            </Row>
        </div>
    );
}

PureCardImage.defaultProps = {
    button: 'View',
}

PureCardImage.propTypes = {
    button: PropTypes.string,
    link: PropTypes.string,
}

// Attach the data HoC to the pure component
export const CardImage = compose(
  pure,
)(PureCardImage);
