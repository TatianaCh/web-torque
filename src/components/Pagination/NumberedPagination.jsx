import React from 'react';
import { withReducer, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import * as Rambda from 'rambda';
import { Crumb } from './Crumb';
import style from './SliderPagination.scss';

const PureNumberedPaginationPagination = ({displayPage, currentPage, totalPage, onNext, onBack, onGoto}) => {


    const maxStartPage = Math.max(totalPage - displayPage, 0)
    const startPage = Math.min(Math.max(0, Math.floor(currentPage-(displayPage-1)/2)), maxStartPage)
    const endPage = Math.min(totalPage, startPage+displayPage)

    const crumbs = []
    for (let i = startPage; i < endPage; i++) {
        crumbs.push((
            <Crumb
                className={style.paginationRadio}
                active={i == currentPage}
                key={i}
                onClick={ _ => onGoto(i) }
                showNumbers
                number={i + 1} />
            ))
    }

    return (
    <Pagination className={style.pagination}>
        {currentPage > 0 && (<PaginationItem  className={style.paginationArrow} onClick={ onBack }>
            <PaginationLink previous className={style.paginationPrev} />
        </PaginationItem>)}
        {crumbs}
        {(currentPage < totalPage-1) && (<PaginationItem className={style.paginationArrow} onClick={ onNext }>
            <PaginationLink next className={style.paginationNext} />
        </PaginationItem>)}
    </Pagination>
)};

PureNumberedPaginationPagination.defaultProps = {
    displayPage: 7,
    currentPage: 0,
    totalPage:1
}

PureNumberedPaginationPagination.propTypes = {
    displayPage: PropTypes.number,
    currentPage: PropTypes.number,
    totalPage: PropTypes.number,
    onNext: PropTypes.func,
    onBack: PropTypes.func,
    onGoto: PropTypes.func,
}

/**
 * Pagination to display results in a `Slider` fashion. Page are not numbered and a limited number of pages can be
 * reached. Once you past the last page you will cycle back to the first one.
 */
export const NumberedPagination = compose(
    pure
)(PureNumberedPaginationPagination);
