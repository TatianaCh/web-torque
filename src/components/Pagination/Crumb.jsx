import React from 'react';
import { withReducer, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { PaginationItem, PaginationLink } from 'reactstrap';
import styles from './Crumb.scss';
import classNames from 'classnames';

export const Crumb = ({active, onClick, showNumbers, number}) => {

    return (
        <PaginationItem className={classNames({[styles.Crumb]:true, [styles.numberCrumb]:showNumbers, [styles.Active]: active})} active={active}>
            <PaginationLink onClick={ onClick }>
                <div role="button" className={styles.Circle}>
                    {!showNumbers && <div className={styles.InnerCircle} />}
                    {showNumbers && <div className={styles.InnerNumber}>{number}</div>}
                </div>
            </PaginationLink>
        </PaginationItem>
    );
};

Crumb.defaultProps = {
    active: false,
    showNumbers: false,
    number: 1,
};

Crumb.propTypes = {
    active: PropTypes.bool,
    onClick: PropTypes.func,
    showNumbers: PropTypes.bool,
    number: PropTypes.number,
};
