import React from 'react';
import { withReducer, pure, compose } from 'recompose';
import PropTypes from 'prop-types';
import { Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import * as Rambda from 'rambda';
import { Crumb } from './Crumb';
import style from './SliderPagination.scss';

const PureSliderPagination = ({maxPage, currentPage, onNext, onBack, onGoto, cycle}) => {
    return (
    <Pagination className={style.pagination}>
        <PaginationItem disabled={!cycle && currentPage == 0} className={style.paginationArrow} onClick={ onBack }>
            <PaginationLink previous className={style.paginationPrev} />
        </PaginationItem>
        {Rambda.times( i => (
            <Crumb
                className={style.paginationRadio}
                active={i == currentPage}
                key={ i }
                onClick={ _ => onGoto(i) }
                showNumbers={false}
                number={i + 1} />
        ), maxPage)}
        <PaginationItem disabled={!cycle && currentPage == maxPage-1} className={style.paginationArrow} onClick={ onNext }>
            <PaginationLink next className={style.paginationNext} />
        </PaginationItem>
    </Pagination>
)};

PureSliderPagination.defaultProps = {
    maxPage: 4,
    currentPage: 0,
    cycle: true
}

PureSliderPagination.propTypes = {
    maxPage: PropTypes.number,
    currentPage: PropTypes.number,
    onNext: PropTypes.func,
    onBack: PropTypes.func,
    onGoto: PropTypes.func,
    cycle: PropTypes.bool
}

/**
 * Pagination to display results in a `Slider` fashion. Page are not numbered and a limited number of pages can be
 * reached. Once you past the last page you will cycle back to the first one.
 */
export const SliderPagination = compose(
    pure
)(PureSliderPagination);
