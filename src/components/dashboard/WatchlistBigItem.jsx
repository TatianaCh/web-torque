import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { cutText } from '../../utils';
import s from './Dashboard.scss';


export class WatchlistBigItem extends React.Component {
    render() {

        const { data } = this.props;
        const imageStyle = {
            backgroundImage: `url(${data.PreviewThumbnailURL})`,
        };
        return (
            <div className={`${s.dashboardItem} ${s.watchListBigItem}`}>
                <div className={s.listingPhotoContainer}>
                    <div className={s.listingImage} style={imageStyle} />
                </div>
                <div className={s.listingInfo}>
                    <div className={s.listingTitle}><span>{data.Title}</span><span className={s.carYear}>{data.SubTitle}</span></div>
                    <div className={s.listingRegion}>{data.Region}</div>
                    <div className={s.listingSummary}>{cutText(data.Summary, 150)}</div>
                </div>
                <div className={s.watchListItemControls}>
                    <Button color="primary"><Link to={data.Url}>view</Link></Button>
                    <Button color="primary">delete</Button>
                </div>
            </div>
        );
    }
}

WatchlistBigItem.propTypes = {
    data: PropTypes.object,
};

WatchlistBigItem.defaultProps = {
    data: {},
};
