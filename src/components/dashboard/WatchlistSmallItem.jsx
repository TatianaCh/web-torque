import React from 'react';
import PropTypes from 'prop-types';
import { cutText } from '../../utils';
import s from './Dashboard.scss';


export class WatchlistSmallItem extends React.Component {
    render() {
        const { data } = this.props;
        const imageStyle = { 
            backgroundImage: `url(${data.PreviewThumbnailURL})`,
        };
        return (
            <div className={s.dashboardItem}>
                <div className={s.listingPhotoContainer}>
                    <div className={s.listingImage} style={imageStyle} />
                </div>
                <div className={s.listingInfo}>
                    <div className={s.listingTitle}><span>{data.Title}</span><span className={s.carYear}>{data.SubTitle}</span></div>
                    <div className={s.listingRegion}>{data.Region}</div>
                    <div className={s.listingSummary}>{cutText(data.Summary, 70)}</div>
                </div>
            </div>
        );
    }
}

WatchlistSmallItem.propTypes = {
    data: PropTypes.object,
};

WatchlistSmallItem.defaultProps = {
    data: {},
};
