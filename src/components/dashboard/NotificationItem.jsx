import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { cutText, isNumeric } from '../../utils';
import s from './Dashboard.scss';

export class NotificationItem extends React.Component {
    render() {
        const { data } = this.props;
        let itemIcon;
        let notifAction;
        let notifValue = false;
        switch (data.notification_type) {
            case 'bid':
                itemIcon = (<span>$</span>);
                notifAction = 'bid on';
                let bidValue = isNumeric(data.value) ? parseFloat(data.value) : 0;
                bidValue = bidValue.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                notifValue = (<span className={s.notifValue}>$ {bidValue}</span>);
                break;
            case 'vote':
                if (data.value) {
                    itemIcon = (<i className="fa fa-thumbs-up" aria-hidden="true" />);
                } else {
                    itemIcon = (<i className="fa fa-thumbs-down" aria-hidden="true" />);
                }
                notifAction = 'vote on';
                break;
            default:
                itemIcon = (<span>?</span>);
                notifAction = 'ask a question about';
                notifValue = (<span className={s.notifValue}>: "{data.value}"</span>);
        }
        return (
            <div className={s.dashboardItem}>
                <div className={s.notifIcon}>
                    {itemIcon}
                </div>
                <div className={s.notifInfo}>
                    <span className={s.userName}>{data.authorName}</span>
                    {notifAction}
                    <span className={s.listingName}>{data.listingTitle} {data.listingSubTitle}</span>
                    {notifValue}
                    <div className={s.notifDate}>{moment(data.Date).format('MMM D')}</div>
                </div>
            </div>
        );
    }
}

NotificationItem.propTypes = {
    data: PropTypes.object,
};

NotificationItem.defaultProps = {
    data: {},
};
