import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { WatchlistBigItem } from '../';
import s from './Dashboard.scss';

export class DashboardWatchList extends React.Component {
    render() {
        const { watchlist } = this.props;
        const watchlistItems = watchlist.map(item => (<WatchlistBigItem data={item} key={item.ID} />));
        if (!watchlistItems.length) {
            watchlistItems.push(<p key="no-items">Your watchlist is empty yet!</p>);
        }
        return (
            <Row>
                <Col lg={12} md={12}>
                    <div className={s.watchListContent}>
                        <div className={s.dashboardTileTitle}>Watchlist ({watchlist.length})</div>
                        {watchlistItems}
                    </div>
                </Col>
            </Row>
        );
    }
}

DashboardWatchList.propTypes = {
    watchlist: PropTypes.array,
};

DashboardWatchList.defaultProps = {
    watchlist: [],
};
