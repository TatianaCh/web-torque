import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Row, Col, Button } from 'reactstrap';
import { WatchlistSmallItem, ListingSmallItem, NotificationItem, Icon } from '../';
import s from './Dashboard.scss';

export class DashboardMainView extends React.Component {
    generateProfileBlock() {
        const { profile } = this.props;
        const avatarStyle = {
            backgroundImage: `url(${profile.PreviewThumbnailURL})`,
        };
        const awards = [];
        if (profile.mate_award) {
            awards.push(
                <div className={s.awardTag} key="mate_award">
                    <i className="fa fa-thumbs-up" aria-hidden="true" />
                    <span>Good Mate</span>
                </div>
            );
        }
        if (profile.racing_award) {
            awards.push(
                <div className={s.awardTag} key="racing_award">
                    <Icon name='flag' height={16} width={16} color="blue" />
                    <span>Racing Enthusiast</span>
                </div>
            );
        }
        if (profile.mechanic_award) {
            awards.push(
                <div className={s.awardTag} key="mechanic_award">
                    <Icon name='tool' height={16} width={16} color="blue" />
                    <span>Certified Mechanic</span>
                </div>
            );
        }
        return (
            <div className={s.dashboardItem}>
                <div className={s.profileAvatar} style={avatarStyle} />
                <div className={s.profileInfo}>
                    <div>{profile.Name}</div>
                    <div className={s.awards}>
                        {awards}
                    </div>
                </div>
            </div>
        );
    }
    render() {
        const { listings, watchlist } = this.props;
        const notifications = [...this.props.notifications];

        const watchlistItems = watchlist.map(item => (<WatchlistSmallItem data={item} key={item.ID} />));
        if (!watchlistItems.length) {
            watchlistItems.push(<p key="no-items">Your watchlist is empty yet!</p>);
        }
        const listingItems = listings.map(item => (<ListingSmallItem data={item} key={item.ID} />));
        if (!listingItems.length) {
            listingItems.push(<p key="no-items">There are no listings yet!</p>);
        }
        notifications.sort((item1, item2) => (moment(item2.Date).diff(moment(item1.Date))));
        const notificationsItems = notifications.map(item => (<NotificationItem data={item} key={item.ID} />));
        if (!notificationsItems.length) {
            notificationsItems.push(<p key="no-items">There are no notifications yet!</p>);
        }
        const profileBlock = this.generateProfileBlock();
        return (
            <div>
                <div className={s.ContentSection}>
                    <div className={s.watchlistBlock}>
                        <div className={s.dashboardTileBlock}>
                            <div className={s.dashboardTileTitle}>Watchlist ({watchlist.length})</div>
                            {watchlistItems}
                            <div className={s.dashboardTileFooter}>
                                <Button color="primary"><Link to="dashboard/watchlist">manage your watchlist</Link></Button>
                            </div>
                        </div>
                    </div>
                    <div className={s.notificationsBlock}>
                        <div className={s.dashboardTileBlock}>
                            <div className={s.dashboardTileTitle}>Notifications ({notifications.length})</div>
                            {notificationsItems}
                            <div className={s.dashboardTileFooter}>
                                <Button color="primary">see all notifications</Button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={s.ContentSection}>
                    <div className={s.watchlistBlock}>
                        <div className={s.dashboardTileBlock}>
                            <div className={s.dashboardTileTitle}>Your active listings ({listings.length})</div>
                            {listingItems}
                            <div className={s.dashboardTileFooter}>
                                <Button color="primary">manage your listings</Button>
                            </div>
                        </div>
                    </div>
                    <div className={s.notificationsBlock}>
                        <div className={s.dashboardTileBlock}>
                            <div className={s.dashboardTileTitle}>Your Profile</div>
                            {profileBlock}
                            <div className={s.dashboardTileFooter}>
                                <Button color="primary">view your public profile</Button>
                                <Button color="primary">edit your profile</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

DashboardMainView.propTypes = {
    profile: PropTypes.object,
    notifications: PropTypes.array,
    listings: PropTypes.array,
    watchlist: PropTypes.array,
};

DashboardMainView.defaultProps = {
    profile: {},
    notifications: [],
    listings: [],
    watchlist: [],
};
