import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button, Container } from 'reactstrap';
import { Icon, ImagesListHOC, VideosListHOC } from '../../';
import styles from './MediaFiles.scss';


export class MediaFiles extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data, onShowImg } = this.props;

        return (
            <Container className={styles.mediaContainer}>
                <Row>
                    <Col lg='6' xs='12'>
                        <ImagesListHOC data={data.Images} onShow={onShowImg} itemClassName={styles.carImg} limit={1} lg={12} md={12} sm={12} xs={12} showBorder={false} />
                    </Col>
                    <Col lg='6' xs='12' className={styles.contentBlock}>
                        <VideosListHOC data={{ items: data.Videos.items }} itemClassName={styles.videoImg} limit={1} lg={12} md={12} sm={12} xs={12} />
                    </Col>
                </Row>
            </Container>
        );
    }
}

MediaFiles.defaultProps = {
    data: {},
};

MediaFiles.propTypes = {
    data: PropTypes.object,
};
