import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Container, Button } from 'reactstrap';
import { Icon, ShortListingInfo } from '../../';
import { getFormattedNumber } from '../../../utils';
import styles from './AboutSellingCar.scss';


export class AboutSellingCar extends React.Component {
    render() {
        const { data, onShowImg } = this.props;
        const closeDateFormat = 'ha, dddd, Do MMMM';
        return (
            <Container className={styles.AboutBlock}>
                <div className={styles.tableContainer}>
                    <span className={styles.title}>car information</span>
                    <table className={`gradient-table ${styles.details}`}>
                        <tbody>
                            <tr>
                                <td>Number plate</td>
                                <td>{data.Number_plate}</td>
                            </tr>
                            <tr>
                                <td>Kilometers</td>
                                <td>{getFormattedNumber(data.Kilometers, 0)}km</td>
                            </tr>
                            <tr>
                                <td>Body</td>
                                <td>{data.Body}</td>
                            </tr>
                            <tr>
                                <td>Fuel Type</td>
                                <td>{data.Fuel}</td>
                            </tr>
                            <tr>
                                <td>Engine</td>
                                <td>{data.Engine}</td>
                            </tr>
                            <tr>
                                <td>Transmission</td>
                                <td>{data.Transmission}</td>
                            </tr>
                            <tr>
                                <td>Import History</td>
                                <td>{data.History}</td>
                            </tr>
                            <tr>
                                <td>Reg Expires</td>
                                <td>{moment(data.Reg_exp).format('MMM YYYY')}</td>
                            </tr>
                            <tr>
                                <td>Wof Expires</td>
                                <td>{moment(data.Wof_exp).format('MMM YYYY')}</td>
                            </tr>
                            <tr>
                                <td>Damage</td>
                                <td>{data.Damage}</td>
                            </tr>
                            <tr>
                                <td>Modifications</td>
                                <td>{data.Modifications}</td>
                            </tr>
                            <tr>
                                <td>Features</td>
                                <td>{data.Features}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div className={styles.infoBlock}>
                    <div>
                        <span className={styles.title}>Subheading</span>
                        <div className={styles.contentBlock}><div dangerouslySetInnerHTML={{__html: data.Description}} /></div>
                    </div>
                    <ShortListingInfo data={data} />
                </div>
            </Container>
        );
    }
}

AboutSellingCar.defaultProps = {
    data: {},
};

AboutSellingCar.propTypes = {
    data: PropTypes.object,
};
