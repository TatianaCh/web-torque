import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Container } from 'reactstrap';
import { Icon, AskQuestion } from '../../';
import { FormatDate } from '../../../utils';
import styles from './Questions.scss';


export class Questions extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data } = this.props;
        const question = data.map(item => (
            <div className={styles.listItem} key={item.id}>
                <div className={styles.questionBlock}>
                    <div className={styles.titleBlock}>Q</div>
                    <div className={styles.textBlock}>
                     <p className={styles.question}>{item.text}</p>
                     <div className={styles.questionInfo}>
                         <span className={styles.name}>{item.user.Name}</span>
                         <span className={styles.speed}><Icon color="blue" height={20} width={20} name='speed' /> {item.user.ReviewsCounter}</span>
                         <span className={styles.questionDate}>{moment(item.date).format('hh:mm A, ddd D MMM')}</span>
                     </div>
                    </div>
                </div>
                <div className={styles.answerSectionBlock}>
                    <div className={styles.titleBlock}>A</div>
                    <div className={styles.textBlock}>{item.answer}</div>
                </div>
            </div>
        ));
        return (
            <Container className={styles.containerBlock}>
                <div className={styles.questionsList}>
                    <span className={styles.title}>questions & answers</span>
                    {question}
                </div>
                <AskQuestion />
            </Container>
        );
    }
}

Questions.defaultProps = {
    data: [],
};

Questions.propTypes = {
    data: PropTypes.array,
};
