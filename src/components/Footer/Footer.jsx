import React from 'react';
import { withState, pure, compose } from 'recompose';
import { Row, Container, Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem, NavLink  } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Icon } from '../';
import styles from './Footer.scss';

const PureFooter = ({routes, open, setOpen, useRouter}) => {
    const links = routes.map(({to, label, icon, icon_width}, i) => {
        const iconProperties = {
            color: 'blue',
            height: 22,
            name: icon,
        };
        if (icon_width) {
            iconProperties.width = icon_width * 1.4;
        }
        return (
            <NavItem key={icon}>
                {useRouter ? (
                    <Link to={to} className="nav-link">
                        {icon && <Icon {...iconProperties} /> }
                        {label}
                    </Link>
                ) : (
                    <NavLink href={to}>
                        {icon && <Icon {...iconProperties} /> }
                        {label}
                    </NavLink>
                )}
            </NavItem>
        );
    });
    return (
    <footer className={styles.Footer}>
        <section className={styles.contentFooter}>
            <Container className={styles.logoImg}>
                <NavbarBrand href="/" className={styles.navBarBrand}>
                    <Icon name="logo_text" className2={styles.textIcon} />
                    <Icon name="logo_img" className2={styles.logoIcon} />
                </NavbarBrand>
                <div className={styles.borderNav} />
            </Container>
            <Container className={styles.MainNav}>
                <div className={styles.navPrivacy}>
                    <a href="#">Vehicle Data Privacy</a>
                    <a href="#">Privacy Policy</a>
                    <a href="#">Legal Terms and Conditions</a>
                </div>
                <div className={styles.navItems}>
                    <Nav>
                        {links}
                    </Nav>
                </div>
                <div className={styles.navSocial}>
                    <div className={styles.positinSocila}>
                        <span className={styles.titleSocial}>SOCIAL</span>
                        <a href="#"><Icon name="twitter" /></a>
                        <a href="#"><Icon name="facebook" /></a>
                    </div>
                </div>
            </Container>
            <Container className={styles.navAbout}>
                <a href="#">FEATURES</a>
                <a href="#">PRICING</a>
                <a href="#">ABOUT</a>
            </Container>
        </section>
        <Container className={styles.Reserved}>
            <p>© Copyright SmartCookie8.com - All Rights Reserved</p>
        </Container>
    </footer>
);
}

PureFooter.defaultProps = {
    useRouter: false,
}

export const Footer = compose(
    pure
)(PureFooter);
