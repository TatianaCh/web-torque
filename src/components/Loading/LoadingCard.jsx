import React from 'react';
import { Card, CardBody } from 'reactstrap';
import ContentLoader, { Rect, Circle } from 'react-content-loader'

// Build a list of rectangles to display i nthe card
const rectangles = [
    {height: 160, width: 1, pad: 10},
    {height: 20, width: .8 , pad: 10},
    {height: 15, width: .4, pad: 10},
    {height: 10, width: 1, pad: 5},
    {height: 10, width: 1, pad: 5},
    {height: 10, width: 1, pad: 5},
    {height: 10, width: 1, pad: 5},
    {height: 10, width: .7, pad: 5},
].reduce((accumulator, rect) => {

    accumulator.elements.push(
        <Rect
            x={0} y={accumulator.height}
            key={accumulator.key++}
            height={rect.height}
            radius={2}
            width={rect.width * 100 + '%'} />
    );
    accumulator.height += rect.height + rect.pad;
    return accumulator;
}, {elements:[], height: 0, key: 0})


// Display a fake Bootstrap card with place holder content.
export const LoadingCard = () => (
<Card>
    <CardBody>
        <ContentLoader height={300} speed={1} width='100%'>
            {rectangles.elements}
        </ContentLoader>
    </CardBody>
</Card>
);
