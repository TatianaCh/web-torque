import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {mapPlaceStyle, mapTextStyle} from './mapPlaceStyle.js';

export default class MapPlace extends Component {
  static defaultProps = {};
  
  render() {
    return (
        <div>
          <div style={mapPlaceStyle}>
          </div>

          <div style={mapTextStyle}>
          {this.props.text}
          </div>
        </div>
    );
  }
}

MapPlace.propTypes = {
  text: PropTypes.string
};