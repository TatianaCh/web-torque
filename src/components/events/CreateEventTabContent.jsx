import React from 'react';
import PropTypes from 'prop-types';
import { Button, Col,Container } from 'reactstrap';
import { Step1, Step2, Step3, Step4, Step5 } from './CreateEvent';
import { SectionHeader } from '../';
import { Icon } from '../';
import classnames from 'classnames';
import { cardTypes } from '../../constants';
import styles from './CreateEventTabContent.scss';


export class CreateEventTabContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 1,
            data: {},
        };
    }

    onChangeStepHandler = (step, newData) => () => {
        const { data } = this.state;
        const newState = this.state;
        newState.step = step;
        if (newData) {
            newState.data = { ...data, ...newData };
        }
        this.setState(newState);
    }

    render() {
        const { step } = this.state;
        const { dictionaries } = this.props;
        let mainContent = false;
        switch (step) {
            case 5: mainContent = (<Step5 nextStep={this.onChangeStepHandler(1)} />);
                break;
            case 4: mainContent = (<Step4 nextStep={this.onChangeStepHandler(5)} />);
                break;
            case 3: mainContent = (<Step3 nextStep={this.onChangeStepHandler(4)} />);
                break;
            case 2: mainContent = (<Step2 nextStep={this.onChangeStepHandler(3)} dictionaries={dictionaries} />);
                break;
            default: mainContent = (<Step1 nextStep={this.onChangeStepHandler(2)} />);
        }
        return (
            <Container>
                <SectionHeader icon={<Icon name='carfront' height={32} width={32} />} children='CREATE AN EVENT' />
                <div className={styles.eventCreate}>
                    <div className={styles.navlinksSection}>
                        <h4 className={styles.title}>create an event</h4>
                        <ul className={styles.navlinksList}>
                            <li><a className={classnames({[styles.Active] : step === 1 })} onClick={this.onChangeStepHandler(1)}>event details</a></li>
                            <li><a className={classnames({[styles.Active] : step === 2 })} onClick={this.onChangeStepHandler(2)}>event requirements</a></li>
                            <li><a className={classnames({[styles.Active] : step === 3 })} onClick={this.onChangeStepHandler(3)}>event schedule</a></li>
                            <li><a className={classnames({[styles.Active] : step === 4 })} onClick={this.onChangeStepHandler(4)}>event promotional material</a></li>
                            <li><a className={classnames({[styles.Active] : step === 5 })} onClick={this.onChangeStepHandler(5)}>event sponsors</a></li>
                        </ul>
                    </div>
                    <div className={styles.contentSection}>
                        {mainContent}
                    </div>
                    <div className={styles.buttonSection}>
                        <Button color="primary">PUBLISH</Button>
                        <Button color="primary">PREVIEW</Button>
                    </div>
                </div>
            </Container>
        );
    }
}

CreateEventTabContent.defaultProps = {
    dictionaries: {},
};

CreateEventTabContent.propTypes = {
    dictionaries: PropTypes.object,
};
