
const K_WIDTH = 40;
const K_HEIGHT = 40;


export const mapPlaceStyle = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates
  position: 'absolute',
  width: 2,
  height: 2,
  left: 0,
  top: 0,
  padding: 4,

  border: '5px solid #F75448',
  borderRadius: K_HEIGHT,
  backgroundColor: '#5C1410',
  textAlign: 'center',
  color: '#3f51b5',
  fontSize: 16,
  fontWeight: 'bold',
  wordBreak: 'normal'
};

export const mapTextStyle = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates
  position: 'absolute',
  height: 'auto',
  left: '10px',
  top: '10px',
  padding: 4,
  width: '150px',
  color: '#3f51b5',
  fontSize: 16,
  fontWeight: 'bold',
  wordBreak: 'normal'
};


