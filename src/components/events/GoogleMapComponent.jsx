import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GoogleMap from 'google-map-react';

import MyGreatPlace from './MapPlace';

export class GoogleMapComponent extends Component {
  
  static defaultProps = {
    text: '',
    center: [-36.848461, 174.763336],
    zoom: 12,
    greatPlaceCoords: {lat: -36.848461, lng: 174.763336}
  };

  constructor(props) {
    super(props);
  }
  
  render() {
    const { lat, lon, center, zoom, text } = this.props;
    let mapCenter = [];

    if (lat && lon) {
      mapCenter = [lat, lon];
    } else {
      mapCenter = center;
    }
    
    return (
       <GoogleMap
        bootstrapURLKeys={{ key: "AIzaSyBf9DoijLVzibSsuHtyRHET6rQVU-QBLXY" }}
        defaultCenter={center}
        center={mapCenter}
        defaultZoom={zoom}>
        {lat && lon ?
          <MyGreatPlace lat={lat} lng={lon} text={text} />
          : false}
      </GoogleMap>
    );
  }
}

GoogleMapComponent.propTypes = {
  center: PropTypes.array,
  zoom: PropTypes.number,
  greatPlaceCoords: PropTypes.any,
  text: PropTypes.string,
};


