import React from 'react';
import { Input, Button } from 'reactstrap';
import PropTypes from 'prop-types';
import { Icon } from '../../../components';
import { guid } from '../../../utils';
import styles from '../CreateEventTabContent.scss';

export class Step5 extends React.Component {
    constructor(props) {
        super(props);
        const data = {};
        const newId = guid();
        data[newId] = {Name: '', Url: ''};
        this.onAddClickHandler = this.onAddClickHandler.bind(this);
        this.state = {
            data,
        };
    }
    onAddClickHandler (){
        const { data } = this.state;
        const newId = guid();
        data[newId] = {Name: '', Url: ''};
        this.setState({data});
    }
    onDeleteClickHandler = itemId => () => {
        const { data } = this.state;
        delete data[itemId];
        this.setState({data});
    }
    render() {
        const { data } = this.state;
        const dataLength = Object.keys(data).length;
        const scheduleList = Object.keys(data).map((key) => {
            const deleteButton = (
                <div className={styles.scheduleDel}>
                    <i className="fa fa-trash-o" aria-hidden="true" onClick={this.onDeleteClickHandler(key)} />
                </div>
            );
            return (
                <div className={styles.sponsors} key={key}>
                    <div className={styles.ImageBlock}>
                        <img src="../../../images/bgImgCard.jpg" alt=""/>
                    </div>
                    <div className={styles.sponsorsConAdd}>
                        <div className={styles.nameInput}>
                            <h3 className={styles.dateTitle}>Name</h3>
                            <Input className={styles.blockInput} type="text" name="heading" placeholder="" />
                        </div>
                        <div className={styles.urlInput}>
                            <h3 className={styles.dateTitle}>URL</h3>
                            <Input className={styles.blockInput} type="text" name="heading" placeholder="" />
                        </div>
                    </div>
                    {dataLength > 1 && deleteButton}
                </div>
            );
        });
        return (
            <div className={styles.stepDetails}>
                <h2 className={styles.titleStep}>Event Sponsors</h2>
                {scheduleList}
                <div className={styles.addedButton}>
                    <Button color="primary" onClick={this.onAddClickHandler}>Add another Sponsor</Button>
                </div>
                <div className={styles.messageBlock}>
                    <h3 className={styles.messageTitle}>‘Become a sponsor’ Message</h3>
                    <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                    <p>Let would-be-sponsors know how to get in touch with you and what exposure you can provide for their brand</p>
                </div>
                <div className={styles.updateButtonMaterial}>
                    <Button color="primary" onClick={this.props.nextStep}>Update Promotional Material</Button>
                </div>
            </div>
        );
    }
}

Step5.defaultProps = {
    nextStep: null,
};

Step5.propTypes = {
    nextStep: PropTypes.func,
};
