import React from 'react';
import { Input, Button } from 'reactstrap';
import PropTypes from 'prop-types';
import { Icon } from '../../../components';
import { guid } from '../../../utils';
import styles from '../CreateEventTabContent.scss';

export class Step3 extends React.Component {
    constructor(props) {
        super(props);
        const data = {};
        const newId = guid();
        data[newId] = {Time: '', Description: ''};
        this.onAddClickHandler = this.onAddClickHandler.bind(this);
        this.state = {
            data,
        };
    }
    onAddClickHandler (){
        const { data } = this.state;
        const newId = guid();
        data[newId] = {Time: '', Description: ''};
        this.setState({data});
    }
    onDeleteClickHandler = itemId => () => {
        const { data } = this.state;
        delete data[itemId];
        this.setState({data});
    }
    render() {
        const { data } = this.state;
        const dataLength = Object.keys(data).length;
        const scheduleList = Object.keys(data).map((key) => {
            const deleteButton = (
                <div className={styles.scheduleDel}>
                    <i className="fa fa-trash-o fa-lg" aria-hidden="true" onClick={this.onDeleteClickHandler(key)} />
                </div>
            );
            return (
                <div className={styles.schedule} key={key}>
                    <div className={styles.scheduleAdd}>
                        <Input className={styles.blockInput} type="text" name="heading" placeholder="" />
                    </div>
                    <div className={styles.scheduleConAdd}>
                        <Input className={styles.blockInput} type="textarea" name="heading" placeholder="" />
                    </div>
                    {dataLength > 1 && deleteButton}
                </div>
            );
        });
        return (
            <div className={styles.stepDetails}>
                <h2 className={styles.titleStep}>Event Schedule</h2>
                {scheduleList}
                <div className={styles.addedButton}>
                    <Button color="primary" onClick={this.onAddClickHandler}>Add New time</Button>
                </div>
                <div className={styles.updateButton}>
                    <Button color="primary" onClick={this.props.nextStep}>Update Event Schedule</Button>
                </div>
            </div>
        );
    }
}

Step3.defaultProps = {
    nextStep: null,
};

Step3.propTypes = {
    nextStep: PropTypes.func,
};
