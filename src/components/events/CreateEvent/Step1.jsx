import React from 'react';
import PropTypes from 'prop-types';
import DateTime from 'react-datetime';
import moment from 'moment';
import { Input, Button } from 'reactstrap';
import { DropDown } from '../../';
import { GoogleMapComponent } from '../';
import styles from '../CreateEventTabContent.scss';
import { brands } from '../../../../demo/mocks/brands';

export class Step1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            'Title': '',
            'start_date': null,
            'start_time': null,
            'end_date': null,
            'end_time': null,
        };
    }

    onDateChangeHandler = (field) => (value) => {
        const newState = { ...this.state };
        newState[field] = value;
        this.setState(newState);
    }

    onInputValueChangeHandler = (field) => (event) => {
        const newState = { ...this.state };
        newState[field] = event.target.value;
        this.setState(newState);
    }

    isValidDate = (current) => {
        const today = moment();
        return current.isAfter(today);
    }

    render() {
        const startDate = this.state.start_date;
        const startTime = this.state.start_time;
        const endDate = this.state.end_date;
        const endTime = this.state.end_time;
        const today = moment();
        const dateInputProps = {
            placeholder: 'MM/DD/YYYY h:mm A',
        };
        return (
            <div className={styles.stepDetails}>
                <div className={styles.reviewheadingBlock}>
                    <div className={styles.titleStepBlock}>
                        <h2 className={styles.titleStep}>title</h2>
                        <h4 className={styles.subTitleStep}>MAXIMUM 10 WORDS</h4>
                    </div>
                    <div className={styles.sillyMargin}>
                        <Input
                            className={styles.blockInput}
                            type="text"
                            name="heading"
                            placeholder=""
                            value={this.state.Title}
                            onChange={this.onInputValueChangeHandler('Title')}
                        />
                    </div>
                </div>
                <div className={styles.reviewWriteBlock}>
                    <div className={styles.titleStepBlock}>
                        <h2 className={styles.titleStep}>Description</h2>
                        <h4 className={styles.subTitleStep}>MAXIMUM 100 WORDS</h4>
                    </div>
                    <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                </div>
                <h2 className={styles.titleStep}>Dates and Time</h2>
                <div className={styles.dateBlock}>
                    <div className={styles.dateSection}>
                        <h3 className={styles.dateTitle}>Start Date</h3>
                        <DateTime
                            value={startDate}
                            onChange={this.onDateChangeHandler('start_date')}
                            inputProps={dateInputProps}
                            isValidDate={this.isValidDate}
                            className={styles.datapicker}
                            timeFormat={false}
                        />
                    </div>
                    <div className={styles.dateSection}>
                        <h3 className={styles.dateTitle}>Start Time</h3>
                        <DateTime
                            value={startTime}
                            className={styles.datapicker}
                            onChange={this.onDateChangeHandler('start_time')}
                            dateFormat={false}
                        />
                    </div>
                    <div className={styles.dateSection}>
                        <h3 className={styles.dateTitle}>End Date</h3>
                        <DateTime
                            value={endDate}
                            onChange={this.onDateChangeHandler('end_date')}
                            inputProps={dateInputProps}
                            isValidDate={this.isValidDate}
                            className={styles.datapicker}
                            timeFormat={false}
                        />
                    </div>
                    <div className={styles.dateSection}>
                        <h3 className={styles.dateTitle}>End Time</h3>
                        <DateTime
                            value={endTime}
                            className={styles.datapicker}
                            onChange={this.onDateChangeHandler('end_time')}
                            dateFormat={false}
                        />
                    </div>
                </div>
                <h2 className={styles.titleStep}>Location and address</h2>
                <h3 className={styles.dateTitle}>Location Name</h3>
                <div className={styles.dateSearch}>
                    <div className={styles.sillyMargin}>
                        <Input className={styles.blockInput} type="text" name="heading" placeholder="" />
                    </div>
                    <div className={styles.searchButton}>
                        <Button color="primary">Search</Button>
                    </div>
                </div>
                <h3 className={styles.dateTitle}>Address</h3>
                <div className={styles.mapBlock}>
                    <div className={styles.adressMap} >
                        <Input type="textarea" name="heading" placeholder="" />
                    </div>
                    <div className={styles.map}>
                        <GoogleMapComponent />
                    </div>
                </div>
                <div className={styles.updateButton}>
                    <Button color="primary" onClick={this.props.nextStep}>Update Event Details</Button>
                </div>
            </div>
        );
    }
}

Step1.defaultProps = {
    nextStep: null,
};

Step1.propTypes = {
    nextStep: PropTypes.func,
};
