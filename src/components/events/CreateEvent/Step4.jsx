import React from 'react';
import PropTypes from 'prop-types';
import styles from '../CreateEventTabContent.scss';
import { Icon, HelpButton } from '../../../components';
import { Input, Button } from 'reactstrap';

export class Step4 extends React.Component {
    render() {
        const helpText = 'Irure aliqua dolore nulla est qui et laboris amet. Ipsum non pariatur ipsum esse reprehenderit voluptate consequat adipisicing';
        return (
            <div className={styles.stepDetails}>
                <h2 className={styles.titleStep}>Promotional material <HelpButton description={helpText} /> </h2>
                <div>
                    <span className={styles.UploadBlock}>
                        <input type="file" />
                        <label><i className="fa fa-upload fa-3x" aria-hidden="true" /></label>
                     </span>
                </div>
                <div className={styles.updateButtonMaterial}>
                    <Button color="primary" onClick={this.props.nextStep}>Update Promotional Material</Button>
                </div>
            </div>
        );
    }
}

Step4.defaultProps = {
    nextStep: null,
};

Step4.propTypes = {
    nextStep: PropTypes.func,
};
