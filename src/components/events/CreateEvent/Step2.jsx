import React from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'reactstrap';
import styles from '../CreateEventTabContent.scss';
import { DropDown } from '../../';


export class Step2 extends React.Component {
    render() {
        const { dictionaries } = this.props;
        return (
            <div>
                <div className={`${styles.stepType} ${styles.stepDetails}`}>
                    <div className={styles.eventTypeBlock}>
                        <h2 className={styles.titleStep}>Event Type</h2>
                        <div className={styles.dropdownMargin}>
                            <DropDown
                                label={dictionaries.eventTypes.label}
                                name="eventTypes"
                                labelHidden
                                items={dictionaries.eventTypes.options}
                                placeholder={dictionaries.eventTypes.placeholder}
                            />
                        </div>
                    </div>
                    <div className={styles.reviePriceBlock}>
                        <h2 className={styles.titleStep}>Price</h2>
                        <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                    </div>
                    <div className={styles.AdditionalBlock}>
                        <h2 className={styles.titleStep}>Participant Requirements</h2>
                        <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                        <p>What skill level is this event recommended for?  What equipement should participant bring?</p>
                    </div>
                    <div className={styles.AdditionalBlock}>
                        <h2 className={styles.titleStep}>Contact Details</h2>
                        <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                    </div>
                    <div className={styles.AdditionalBlock}>
                        <h2 className={styles.titleStep}>Additional Details</h2>
                        <div className={styles.sillyMargin}><Input className={styles.blockInput} type="textarea" name="heading" placeholder="" /></div>
                        <p>Are there any other key details participants should now?</p>
                        <p>Are you looking for volunteers? Are spectators welcomed? </p>
                        <p>Are passengers allowed to ride with drivers? Is food provided at the venue?</p>
                    </div>
                    <div className={styles.buttonUpdate}>
                        <Button color="primary" onClick={this.props.nextStep}>Update Event Requirements</Button>
                    </div>
                </div>
            </div>
        );
    }
}

Step2.defaultProps = {
    nextStep: null,
    dictionaries: {},
};

Step2.propTypes = {
    nextStep: PropTypes.func,
    dictionaries: PropTypes.object,
};
