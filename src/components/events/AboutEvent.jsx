import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './AboutEvent.scss';
import { FormatDate } from '../../utils';
import { GoogleMapComponent } from './';

export class AboutEvent extends React.Component {
    render() {
        const { AttributionUrl, Description, sponsors, Start, End, Location, Views, Organizer, Type, Price, p_requirements,
            Contacts, Additional, Signup, Schedule } = this.props.data;
        const sponsor = sponsors.map(item => (
            <a className={styles.sponsorLogo} key={item.id} title={item.name} href={item.url}>
                <img src={item.logo} alt={item.name}/>
            </a>
        ));
        const schedule = Schedule.map((item) => (
            <tr className={styles.scheduleItem} key={item.ID}>
                <td>{moment(item.Time).format('h:mm A')}</td>
                <td>{item.Description}</td>
            </tr>
        ));
        return (
            <div className={styles.AboutBlock}>
                <div className={styles.EventsConBlock}>
                    <div className={styles.contentBlock}>
                        <div dangerouslySetInnerHTML={{__html: Description}} />
                    </div>
                    <h1 className={styles.title}>Event details</h1>
                    <table className={`gradient-table inverted-gradient ${styles.details}`}>
                        <tbody>
                            <tr>
                                <td>Type</td>
                                <td>{Type}</td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td>{Price}</td>
                            </tr>
                            <tr>
                                <td>Participant requirements</td>
                                <td>{p_requirements}</td>
                            </tr>
                            <tr>
                                <td>Contact</td>
                                <td className={styles.detailsContacts}>
                                    <p>{Contacts.phone}</p>
                                    <a href={Contacts.url}>{Contacts.url}</a>
                                    <a href={Contacts.email}>{Contacts.email}</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional Details</td>
                                <td>{Additional}</td>
                            </tr>
                            <tr>
                                <td>Sign Up</td>
                                <td>{Signup}</td>
                            </tr>
                        </tbody>
                    </table>
                    <h1 className={styles.title}>Schedule</h1>
                    <table className={`gradient-table inverted-gradient ${styles.Schedule}`}>
                        <tbody>
                        {schedule}
                        </tbody>
                    </table>
                </div>

                <div className={styles.imagesBlock}>
                    <div className={styles.locationBlock}>
                        <h1 className={styles.title}>Event location</h1>
                        <h3 className={styles.LocationTitle}>{moment(Start).format('dddd MMM D YYYY')}</h3>
                        <h3 className={styles.LocationTitle}>{moment(Start).format('h:mm A')} to {moment(End).format('h:mm A')}</h3>
                        <div className={styles.map}>
                          <GoogleMapComponent
                            text={Location.address}
                            lat={Location.lat}
                            lon={Location.lon}
                        />
                        </div>
                        <h1 className={styles.title}>location name</h1>
                        <h3 className={styles.numberTitle}>{Views} location</h3>
                    </div>
                    <div className={styles.logoBlock}>
                        <h1 className={styles.title}>event sponsors</h1>
                        {sponsor}
                        <h2 className={styles.subTitle}>become a sponsor</h2>

                        <div className={styles.note}>
                            <p>We are still looking for sponsors.</p>
                            <p>Email <a href={AttributionUrl}>john@trackday.co.nz</a> if you are interested.</p>
                        </div>
                    </div>
                    <div className={styles.organisedBlock}>
                        <h1 className={styles.title}>organised by</h1>
                        <div className={styles.organisedLogo}>
                            <div className={styles.logo}>
                                <a className={styles.Organizer} key={Organizer.id} title={Organizer.name} href={Organizer.url}>
                                    <img src={Organizer.logo} alt={Organizer.name}/>
                                </a>
                            </div>
                            <div className={styles.note}>
                                <p>{Organizer.name}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

AboutEvent.defaultProps = {
    data: {},
};

AboutEvent.propTypes = {
    data: PropTypes.object,
};
