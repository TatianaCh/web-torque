import React from 'react';
import PropTypes from 'prop-types';
import { Collapse } from 'reactstrap';
import styles from './AccordionComponent.scss';


export class AccordionComponent extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: props.forceOpen,
        };
    }

    toggle() {
        this.setState({
            isOpen: this.props.forceOpen || !this.state.isOpen,
        });
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            isOpen: this.state.isOpen || newProps.forceOpen,
        })
    }

    render() {
        const { isOpen } = this.state;
        const { children, label, className2, forceOpen } = this.props;
        const accordionBtnText = isOpen ? '-' : '+';
        const containerClassName = className2 ? `${styles.accordionBlock} ${className2}` : styles.accordionBlock;
        return (
            <div className={containerClassName}>
                <div className={styles.accordionTitle + ' ' + (forceOpen ? styles.disabledTitle : '')} onClick={this.toggle}>
                    <h4 className={styles.title}>{label}</h4>
                    <span className={styles.icon}>
                        {accordionBtnText}
                    </span>
                </div>
                <Collapse isOpen={this.state.isOpen}>
                    <div className={styles.collapse}>
                        {children}
                    </div>
                </Collapse>
            </div>
        );
    }
}

AccordionComponent.propTypes = {
    children: PropTypes.node,
    label: PropTypes.string,
    className2: PropTypes.string,
    forceOpen: PropTypes.bool,
}

AccordionComponent.defaultProps = {
    children: null,
    label: '',
    className2: '',
    forceOpen: false,
};
