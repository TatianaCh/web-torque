import React from 'react';
import { Container, Row, Col  } from 'reactstrap';
import _ from 'lodash';
import { SectionHeader, TabList, Tab, DriversLandingTabContent } from '../components';
import { types } from '../../demo/mocks/types';
import { places } from '../../demo/mocks/places';
import { years } from '../../demo/mocks/years';
import { Icon } from '../components';
import data from '../../demo/mocks/DriversResultSet.json';
import { cardTypes } from '../constants';


const rData = _.cloneDeep(data);
rData.items = rData.items.reverse();

const cardType = cardTypes.driver;

const dictionaries = {
    years: {
        label: 'When',
        placeholder: "When",
        options: years,
    },
    types: {
        label: 'Type',
        placeholder: "Type",
        options: types,
    },
    places: {
        label: 'Where',
        placeholder: "Where",
        options: places,
    }
}
export const Drivers = () => (
    <div>
        <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'DRIVERS'}/>
        <TabList>
            <Tab title="BROWSE DRIVERS" icon={<Icon name='glasses' height={20} width={20} />}>
                <DriversLandingTabContent data={data} rData={rData} dictionaries={dictionaries} cardType={cardType} />
            </Tab>
            <Tab title="SPONSOR A DRIVER" icon={<Icon name='glasses' height={20} width={20} />}>
                Review this.
            </Tab>
        </TabList>
    </div>
)
