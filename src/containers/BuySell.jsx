import React from 'react';
import { Row, Col, Container } from 'reactstrap';
import _ from 'lodash';
import { SectionHeader, TabList, Tab, CardListHOC, SimpleSearchForm, BuyCarTabContent, BuyPartsTabContent,
  CarModelBuySell } from '../components';
import { brands } from '../../demo/mocks/brands';
import { years } from '../../demo/mocks/years';
import { types } from '../../demo/mocks/types';
import { Icon } from '../components';
import parts from '../../demo/mocks/BuySellPartsResultSet.json';
import data from '../../demo/mocks/BuySellResultSet.json';
import { cardTypes } from '../constants';
import s from './BuySell.scss';

export class BuySell extends React.Component {
    constructor(props) {
        super(props);
        this.rData = _.cloneDeep(data);
        this.rData.items = this.rData.items.reverse();
        this.rParts = _.cloneDeep(parts);
        this.rParts.items = this.rParts.items.reverse();

        this.searchCarData = {
            brands: {
                label: 'Make',
                placeholder: "Any Make",
                options: brands,
            },
            models: {
                label: 'Model',
                placeholder: "Any Model",
                options: years,
            },
            years: {
                label: 'Years',
                placeholder: "Any Year",
                options: years,
            },
        }

        this.searchPartsData = {
            ...this.searchCarData,
            types: {
                label: 'Types',
                placeholder: "Part Type",
                options: types,
            },
        }
        this.state = {
            selectedTab: 'buyCar',
        };
        this.onChangeTab = this.onChangeTab.bind(this);
    }

    onChangeTab(tabId) {
        if (this.state.selectedTab !== tabId) {
            this.setState({
                selectedTab: tabId,
            });
        }
    }

    render() {
        const { selectedTab } = this.state;
        const hero = './images/engine.png';
        const titleImage = (<img src={hero} alt="" />);

        const cardType = cardTypes.buySell;
        let headerPart = (<SectionHeader icon={<Icon name='wheel' height={36} width={36} />} children='BUY & SELL' hero={titleImage} />);
        if (selectedTab === 'buyCar') {
            headerPart = (<div className={s.ImgBlock} />);
        }
        return (
            <div className={s.sellPage}>
                {headerPart}
                <TabList onChangeTabCallback={this.onChangeTab} active={selectedTab}>
                    <Tab title="BUY A CAR" key="buyCar" icon={<Icon name='key' height={20} width={20} />} >
                        <BuyCarTabContent data={data} rData={this.rData} dictionaries={this.searchCarData} cardType={cardType} />
                    </Tab>
                    <Tab title="BROWSE PARTS" key="buyPart" icon={<Icon name='engine' height={20} width={20} />}>
                        <BuyPartsTabContent data={parts} rData={this.rParts} dictionaries={this.searchPartsData} cardType={cardType}  />
                    </Tab>
                    <Tab title="LIST CARS" key="listCars" icon={<Icon name='key' height={20} width={20} />}>
                        <Container>
                            <CarModelBuySell data={data} showViewAllButton={false} />
                        </Container>
                    </Tab>
                    <Tab title="LIST PARTS" key="listParts" icon={<Icon name='engine' height={20} width={20} />}>
                        <Container>
                            <CarModelBuySell data={parts} isParts showViewAllButton={false} />
                        </Container>
                    </Tab>
                </TabList>
            </div>
        );
    }
}
