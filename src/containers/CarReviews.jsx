import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col  } from 'reactstrap';
import _ from 'lodash';
import { SectionHeader, TabList, Tab, CardListHOC, BrowseCarsTabContent, WriteReviewTabContent } from '../components';
import { Icon } from '../components';
import { brands } from '../../demo/mocks/brands';
import { years } from '../../demo/mocks/years';
import { types } from '../../demo/mocks/types';
import data from '../../demo/mocks/CarReviewResultSet.json';
import { cardTypes } from '../constants';

const hero = './images/cars.png';

const rData = _.cloneDeep(data);
rData.items = rData.items.reverse();

const titleImage = (<img src={hero} alt="" />);
const models = [];

const dictionaries = {
    brands: {
        label: 'Make',
        placeholder: "Any Make",
        options: brands,
    },
    models: {
        label: 'Model',
        placeholder: "Any Model",
        options: years,
    },
    years: {
        label: 'Years',
        placeholder: "Any Year",
        options: years,
    },
    types: {
        label: 'Types',
        placeholder: "Any Type",
        options: types,
    }
};

const categories = {
    cat1: {
        label: 'CATEGORY 1',
        values: {
            'DRIVING FEEL': 3,
            'DRIVING COMFORT': 3,
            'DRIVING POSITION': 3,
            'FRONT SEAT COMFORT': 4,
            'BACK SEAT COMFORT': 5,
            'STORAGE': 3,
        }
    },
    cat2: {
        label: 'CATEGORY 2',
        values: {
            'DRIVING FEEL': 4,
            'DRIVING COMFORT': 5,
            'DRIVING POSITION': 5,
            'FRONT SEAT COMFORT': 5,
            'BACK SEAT COMFORT': 5,
            'STORAGE': 3,
        }
    },
    cat3: {
        label: 'CATEGORY 3',
        values: {
            'DRIVING FEEL': 3,
            'DRIVING COMFORT': 3,
            'DRIVING POSITION': 3,
            'FRONT SEAT COMFORT': 4,
            'BACK SEAT COMFORT': 5,
            'STORAGE': 3,
        }
    },
    cat4: {
        label: 'CATEGORY 4',
        values: {
            'DRIVING FEEL': 3,
            'DRIVING COMFORT': 3,
            'DRIVING POSITION': 3,
            'FRONT SEAT COMFORT': 4,
            'BACK SEAT COMFORT': 5,
            'STORAGE': 3,
        }
    },
    cat5: {
        label: 'CATEGORY 5',
        values: {
            'DRIVING FEEL': 3,
            'DRIVING COMFORT': 3,
            'DRIVING POSITION': 3,
            'FRONT SEAT COMFORT': 4,
            'BACK SEAT COMFORT': 5,
            'STORAGE': 3,
        }
    },
    cat6: {
        label: 'CATEGORY 6',
        values: {
            'DRIVING FEEL': 5,
            'DRIVING COMFORT': 5,
            'DRIVING POSITION': 3,
            'FRONT SEAT COMFORT': 1,
            'BACK SEAT COMFORT': 4,
            'STORAGE': 5,
        }
    }
};

export const CarReviews = () => (
    <div>
        <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'CAR REVIEWS'} hero={titleImage} />
        <TabList>
            <Tab title="BROWSE CARS" icon="glasses">
                <BrowseCarsTabContent data={data} rData={rData} dictionaries={dictionaries} />
            </Tab>
            <Tab title="WRITE A REVIEW" icon="glasses">
                <WriteReviewTabContent dictionaries={dictionaries} categories={categories} title="car details" />
            </Tab>
        </TabList>
    </div>
);
