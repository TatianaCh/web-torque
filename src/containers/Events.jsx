import React from 'react';
import { Row, Col  } from 'reactstrap';
import _ from 'lodash';
import { SectionHeader, TabList, Tab, EventsLandingTabContent, CreateEventTabContent } from '../components';
import { types } from '../../demo/mocks/types';
import { places } from '../../demo/mocks/places';
import { years } from '../../demo/mocks/years';
import { eventTypes } from '../../demo/mocks/eventTypes';
import data from '../../demo/mocks/EventsResultSet.json';
import { Icon } from '../components';
import { cardTypes } from '../constants';

const rData = _.cloneDeep(data);
rData.items = rData.items.reverse();

const models = [];
const cardType = cardTypes.events;

const dictionaries = {
    years: {
        label: 'When',
        placeholder: "When",
        options: years,
    },
    types: {
        label: 'Type',
        placeholder: "Type",
        options: types,
    },
    places: {
        label: 'Where',
        placeholder: "Where",
        options: places,
    },
    eventTypes: {
        label: 'Event Type',
        placeholder: "Event Type",
        options: eventTypes,
    }
}

export const Events = () => (
    <div>
        <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'EVENTS'}/>
        <TabList>
            <Tab title="BROWSE EVENTS" icon={<Icon name='glasses' height={20} width={20} />}>
                <EventsLandingTabContent data={data} rData={rData} dictionaries={dictionaries} cardType={cardType} />
            </Tab>
            <Tab title="CREATE AN EVENT" icon={<Icon name='glasses' height={20} width={20} />}>
                <CreateEventTabContent dictionaries={dictionaries} />
            </Tab>
        </TabList>
    </div>
)
