import React from 'react';
import { RegistrationForm, SectionHeader } from '../components';
import styles from './Login.scss';

export const Register = (children) => (
    <div>
        <SectionHeader children={'REGISTER'} hero={'../images/cars.png'} className2={styles.loginHeader} />
        <RegistrationForm />
    </div>
)
