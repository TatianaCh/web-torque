import React from 'react';
import { Row, Col } from 'reactstrap';
import _ from 'lodash';
import { SectionHeader, TabList, Tab, WorkshopsLandingTabContent, WriteReviewTabContent } from '../components';
import { ratings } from '../../demo/mocks/ratings';
import { regions } from '../../demo/mocks/regions';
import { service } from '../../demo/mocks/service-types';
import { Icon } from '../components';
import data from '../../demo/mocks/WorkShopsResultSet.json';
import { cardTypes } from '../constants';

const hero = './images/workshop.png';

const rData = _.cloneDeep(data);
rData.items = rData.items.reverse();

const titleImage = (<img src={hero} alt="" />);
const cardType = cardTypes.workshop;

const dictionaries = {
    service: {
        label: 'Service',
        placeholder: "Service Type",
        options: service,
    },
    region: {
        label: 'Region',
        placeholder: "Region",
        options: regions,
    },
    ratings: {
        label: 'Rating',
        placeholder: "Rating",
        options: ratings,
    }
}

const categories = {
    cat1: {
        label: 'CATEGORY 1',
        values: {
            'Friendliness': 3,
            'Quality': 3,
            'Cost': 3,
            'Speed': 4,
        }
    },
    cat2: {
        label: 'CATEGORY 2',
        values: {
            'Friendliness': 4,
            'Quality': 4,
            'Cost': 5,
            'Speed': 4,
        }
    },
    cat3: {
        label: 'CATEGORY 3',
        values: {
            'Friendliness': 5,
            'Quality': 5,
            'Cost': 5,
            'Speed': 5,
        }
    },
    cat4: {
        label: 'CATEGORY 4',
        values: {
            'Friendliness': 5,
            'Quality': 4,
            'Cost': 2,
            'Speed': 5,
        }
    },
};

export const Workshop = () => (
    <div>
        <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children={'WORKSHOPS'} hero={titleImage} />
        <TabList>
            <Tab title="BROWSE WORKSHOPS" icon={<Icon name='glasses' height={20} width={20} />}>
                <WorkshopsLandingTabContent data={data} rData={rData} dictionaries={dictionaries} cardType={cardType} />
            </Tab>
            <Tab title="WRITE A REVIEW" icon={<Icon name='glasses' height={20} width={20} />}>
                <WriteReviewTabContent categories={categories} title="workshop review" showSocialLinksInputs={false} />
            </Tab>
        </TabList>

    </div>
)
