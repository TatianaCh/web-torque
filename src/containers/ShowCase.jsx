import React from 'react';
import { Row, Col } from 'reactstrap';

export const ShowCase = ({children, name, state, purpose}) => {
    return (
        <Row>
            <Col sm={8}>{children}</Col>
            <Col sm={4}>
                <h2>{name}</h2>
                <p><em>{state}</em></p>
                <p>{purpose}</p>
                <p>Props:</p>
                <ul>
                {Object.keys(children.props).map(
                    key => <li key={key}>{key}: {
                        (typeof children.props[key] == 'function') ? (<em>Function</em>) : children.props[key]
                    }</li>
                )}
                </ul>

            </Col>
            <hr style={{background: 'gray', width: '100%'}} />
        </Row>
    );
}
