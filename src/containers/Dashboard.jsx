import React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { Row, Col, Container } from 'reactstrap';
import _ from 'lodash';
import { SectionHeader, DashboardMainView, DashboardWatchList, Icon, AccordionComponent } from '../components';
import { isEmptyObject } from '../utils';
import s from './Dashboard.scss';
import users from '../../demo/mocks/UsersResultSet.json';
import buySellData from '../../demo/mocks/BuySellResultSet.json';
import notificationsData from '../../demo/mocks/UsersNotificationsSet.json';

const profileData = users.items[0];
const listingsData = buySellData.items.filter(item => (item.userId === profileData.ID));
const watchListData = [];
profileData.watchlist.forEach((advId) => {
    const advertismentData = buySellData.items.find(item => (item.ID === advId));
    if (!isEmptyObject(advertismentData)) {
        watchListData.push(advertismentData);
    }
});
const notifData = notificationsData.items.filter(item => (item.userId === profileData.ID));

notifData.forEach((notification, index) => {
    const advertismentData = buySellData.items.find(item => (item.ID === notification.listingId));
    if (!isEmptyObject(advertismentData)) {
        notifData[index].listingTitle = advertismentData.Title;
        notifData[index].listingSubTitle = advertismentData.SubTitle;
    }
    const authorData = users.items.find(item => (item.ID === notification.authorId));
    if (!isEmptyObject(authorData)) {
        notifData[index].authorName = authorData.Name;
    }
});

export const Dashboard = (props) => {
    const section = props.match.params.section;
    let mainContent = false;
    switch (section) {
        case 'watchlist':
            mainContent = (<DashboardWatchList watchlist={watchListData} />);
            break;
        default:
            mainContent = (<DashboardMainView profile={profileData} watchlist={watchListData} listings={listingsData} notifications={notifData} />);
    }
    const menuBlock = (
        <div className={s.dashboardMenu}>
            <h3>Buying</h3>
            <ul>
                <li className={classNames({[s.active]: section === 'watchlist'})}><Link to="/dashboard/watchlist">watchlist</Link></li>
                <li><a>items i won</a></li>
                <li><a>items i lost</a></li>
                <li><a>my favourites</a></li>
                <li><a>recently viewed</a></li>
            </ul>
            <h3>Selling</h3>
            <ul>
                <li><a>list an item</a></li>
                <li><a>items in selling</a></li>
                <li><a>sold items</a></li>
                <li><a>unsold items</a></li>
            </ul>
            <h3>Reviews</h3>
            <ul>
                <li><a>my reviews</a></li>
                <li><a>write a review</a></li>
            </ul>
            <h3>Profile</h3>
            <ul>
                <li><a>edit profile</a></li>
                <li><a>view profile</a></li>
            </ul>
            <h3>create an event</h3>
            <ul>
                <li><a>event details</a></li>
                <li><a>event requirements</a></li>
                <li><a>event schedule</a></li>
                <li><a>event promotional material</a></li>
                <li><a>event sponsors</a></li>
            </ul>
        </div>
    );
    return (
        <Container className={s.pageContent}>
            <SectionHeader icon={<Icon name='carfront' height={32} width={32} />} children={'my dashboard'} />
            <div className={s.mainBlock}>
                <div className={s.asideMenu}>
                    {menuBlock}
                </div>
                <div className={s.mobileMenu}>
                    <AccordionComponent className2={s.menuAccordion} label="menu">
                        {menuBlock}
                    </AccordionComponent>
                </div>
                <div className={s.mainContent}>
                    {mainContent}
                </div>
            </div>
        </Container>
    )
};
