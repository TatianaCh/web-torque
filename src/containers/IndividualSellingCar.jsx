import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import { TabList, Tab, ReviewHeader, ImageWidget, AboutDriver, CarModelReviews, SimpleHeader, MediaFiles } from '../components';
import { isEmptyObject } from '../utils';
import { Icon, AboutSellingCar, Questions,MakeBid } from '../components';
import styles from './IndividualSellingCar.scss';
import reviews from '../../demo/mocks/ReviewsResultSet.json';
import users from '../../demo/mocks/UsersResultSet.json';
import bids from '../../demo/mocks/BidsResultSet.json';
import carModelInfo from '../../demo/mocks/IndividualCar.json';
import { FormatDate, getFormattedNumber } from '../utils';

import _ from 'lodash';
const bidsList = _.cloneDeep(bids);
const reviewsContent = _.cloneDeep(reviews);
const reviewsList = [];
reviewsContent.items.forEach((review) => {
    const userInfo = users.items.find(user => user.ID === review.userId);
    reviewsList.push({ ...review, user: userInfo });
});

const carReviewData = {
    rating: carModelInfo.ReviewAverage,
    reviewCount: carModelInfo.ReviewCount,
    reviewsDistribution: carModelInfo.reviewsDistribution,
    charactericsEsteem: carModelInfo.charactericsEsteem,
};

export const IndividualSellingCar = ({data, onShowImg}) => {
    const listingInfo = data.item;
    if (listingInfo.userId) {
        const userInfo = users.items.find(user => user.ID === listingInfo.userId);
        listingInfo.user = userInfo;
    }
    listingInfo.Questions.forEach((question, index) => {
        const userInfo = users.items.find(user => user.ID === question.userId);
        listingInfo.Questions[index].user = userInfo;
    });
    bidsList.items.forEach((bid, index) => {
        const userInfo = users.items.find(user => user.ID === bid.userId);
        bidsList.items[index].user = userInfo;
    });
    listingInfo.bids = bidsList.items;
    return (
        <div className={styles.carModel}>
            <Container>
                <Row className={styles.blockMargin}>
                    <Col xs={12} lg={8}>
                        <div className={styles.titleBlock}>
                            <SimpleHeader>{listingInfo.Brand}, {listingInfo.Model}, {listingInfo.Year}</SimpleHeader>
                            <div className={styles.nearPrice}>
                                <span className={styles.priseSpan}>$ {getFormattedNumber(listingInfo.Price, 0)}</span>
                                <span className={styles.priseInfo}>or near offer</span>
                            </div>
                        </div>
                    </Col>
                    <Col xs={12} lg={4} className={styles.ContentPadding}>
                        <ImageWidget
                            showlabel={false}
                            thumbnailImg={listingInfo.PreviewThumbnailURL}
                            img={listingInfo.PrimaryImageURL}
                            onShow={onShowImg}
                        />
                        <div className={styles.buttonBlock}>
                            <Button color="secondary" className={styles.buttonAsk}>ask a question</Button>
                            <Button color="primary" className={styles.buttonMake}>Make an offer</Button>
                        </div>
                        <span className={styles.infoSpan}>Listing #: {listingInfo.Number}, Closes {FormatDate(listingInfo.End)}</span>
                    </Col>
                </Row>
            </Container>
            <TabList>
                <Tab title="about this car" icon={<Icon name='glasses' height={20} width={20} />}>
                    <AboutSellingCar data={listingInfo} onShowImg={onShowImg} />
                </Tab>
                <Tab title="images & video" icon={<Icon name='glasses' height={20} width={20} />}>
                    <MediaFiles data={listingInfo} />
                </Tab>
                <Tab title="Q & A" icon={<Icon name='glasses' height={20} width={20} />}>
                    <Questions data={listingInfo.Questions} />
                </Tab>
                <Tab title="Make an offer / bid" icon={<Icon name='glasses' height={20} width={20} />}>
                    <MakeBid data={listingInfo} />
                </Tab>
                <Tab title="reviews (123)" icon={<Icon name='glasses' height={20} width={20} />}>
                    <Container>
                        <CarModelReviews
                            reviews={reviewsList}
                            {...carReviewData}
                        />
                    </Container>
                </Tab>
            </TabList>
        </div>
    );
};
