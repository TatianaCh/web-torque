import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { TabList, Tab, ImageWidget, AboutEvent, SimpleHeader, CreateEventTabContent } from '../components';
import { Icon } from '../components';
import styles from '../components/Layout/LargeSection.scss';
import { types } from '../../demo/mocks/types';
import { places } from '../../demo/mocks/places';
import { years } from '../../demo/mocks/years';
import { eventTypes } from '../../demo/mocks/eventTypes';

import _ from 'lodash';
const dictionaries = {
    years: {
        label: 'When',
        placeholder: "When",
        options: years,
    },
    types: {
        label: 'Type',
        placeholder: "Type",
        options: types,
    },
    places: {
        label: 'Where',
        placeholder: "Where",
        options: places,
    },
    eventTypes: {
        label: 'Event Type',
        placeholder: "Event Type",
        options: eventTypes,
    }
}

export const IndividualEvent = ({data: {item}}) => {
    return (
        <div className={styles.carModel}>
            <Container>
                <Row className={styles.blockMargin}>
                    <Col xs={12} lg={7}>
                        <div className={styles.titleBlockEvent}>
                            <SimpleHeader >{ item.Title }</SimpleHeader>
                        </div>
                    </Col>
                    <Col xs={12} lg={5} className={styles.ContentPadding}>
                        <ImageWidget className2={true} thumbnailImg={item.PreviewThumbnailURL} img={item.PrimaryImageURL} />
                    </Col>
                </Row>
            </Container>
            <TabList>
                <Tab title="browse events" icon={<Icon name='glasses' height={20} width={20} />}>
                    <Container>
                        <AboutEvent data={item} />
                    </Container>
                </Tab>
                <Tab title="create an event" icon={<Icon name='glasses' height={20} width={20} />}>
                    <CreateEventTabContent dictionaries={dictionaries} />
                </Tab>
            </TabList>
        </div>
    );
};
