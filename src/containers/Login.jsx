import React from 'react';
import { LoginForm, SectionHeader } from '../components';
import styles from './Login.scss';

export const Login = (children) => (
    <div>
        <SectionHeader children={'LOGIN'} hero={'../images/cars.png'} className2={styles.loginHeader} />
        <LoginForm />
    </div>
)
