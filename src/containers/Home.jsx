import React from 'react';
import { Row, Col } from 'reactstrap';
import _ from 'lodash';
import { LoadingCard, HomeSearch, HomePassion, HomeContent, HomeNavigation, SectionHeader, Icon, TabList, Tab, CardListHOC } from '../components';
import topData from '../../demo/mocks/CarReviewResultSet.json';
import carData from '../../demo/mocks/BuySellResultSet.json';
import { categories } from '../../demo/mocks/categories';
import routes from '../routes.json';
import { cardTypes } from '../constants';

const hero = './images/cars.png';
const titleImage = (<img src={hero} alt="" />);

const hero2 = './images/car-frame.png';
const titleImage2 = (<img src={hero2} alt="" />);

const rTopData = _.cloneDeep(topData);
rTopData.items = rTopData.items.reverse();
const rCarData = _.cloneDeep(carData);
rCarData.items = rCarData.items.reverse();

export const Home = (props) => {
	return (
	    <div>
	        <HomeNavigation routes={routes} useRouter />
            <HomeSearch categories={categories} />
	        <SectionHeader icon={<Icon name='edit_list' height={32} width={32} />} children='TOP REVIEWS' hero={titleImage} />
	        <TabList>
	            <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
	                <CardListHOC data={topData} cardType={cardTypes.carReviews} />
	            </Tab>
	            <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
	                <CardListHOC data={rTopData} cardType={cardTypes.carReviews} />
	            </Tab>
	        </TabList>

	        <SectionHeader icon={<Icon name='carfront' height={32} width={32} />} children='TOP CAR LISTINGS' hero={titleImage2} />
	        <TabList>
	            <Tab title="TOP RATED" icon={<Icon name='speed' height={20} width={20} />}>
	                <CardListHOC data={carData} cardType={cardTypes.buySell} />
	            </Tab>
	            <Tab title="MOST VIEWED" icon={<Icon name='glasses' height={20} width={20} />}>
	                <CardListHOC data={rCarData} cardType={cardTypes.buySell} />
	            </Tab>
	        </TabList>
            <HomeContent />
            <HomePassion />
	    </div>
	);
}
