import React from 'react';
import { Container, Row, Col  } from 'reactstrap';
import { TabList, Tab, ReviewHeader, ImageWidget, AboutDriver, SimpleHeader } from '../components';
import { isEmptyObject } from '../utils';
import { Icon } from '../components';
import styles from './IndividualDriver.scss';
import styles2 from '../components/Layout/LargeSection.scss';

import _ from 'lodash';

export const IndividualDriver = ({data: {item}, onShowImg}) => {
    return (
        <div className={styles2.carModel}>
            <Container>
                <Row className={styles2.blockMargin}>
                    <Col xs={12} lg={7}>
                        <div className={styles2.titleBlock}>
                            <SimpleHeader >{ item.Title }</SimpleHeader>
                        </div>
                    </Col>
                    <Col xs={12} lg={5} className={styles2.ContentPadding}>
                        <ImageWidget
                            className2={styles.ImageWidgetDriver}
                            thumbnailImg={item.PreviewThumbnailURL}
                            img={item.PrimaryImageURL}
                            onShow={onShowImg}
                        />
                    </Col>
                </Row>
            </Container>
            <TabList>
                <Tab title="browse Driver" icon={<Icon name='glasses' height={20} width={20} />}>
                    <Container>
                        <AboutDriver data={item} onShowImg={onShowImg} />
                    </Container>
                </Tab>
                <Tab title="sponsor a driver" icon={<Icon name='glasses' height={20} width={20} />}>
                    sponsor
                </Tab>
            </TabList>
        </div>
    );
};
