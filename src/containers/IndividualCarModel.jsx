import React from 'react';
import { Container, Row, Col  } from 'reactstrap';
import { TabList, Tab, ReviewHeader, ImageWidget, AboutCar, CarModelReviews, CarModelBuySell } from '../components';
import { isEmptyObject } from '../utils';
import reviews from '../../demo/mocks/ReviewsResultSet.json';
import users from '../../demo/mocks/UsersResultSet.json';
import sellCar from '../../demo/mocks/BuySellResultSet.json';
import sellCarParts from '../../demo/mocks/BuySellPartsResultSet.json';
import { Icon } from '../components';
import styles from '../components/Layout/LargeSection.scss';

import _ from 'lodash';

const sellCarContent = _.cloneDeep(sellCar);
const sellCarPartsContent = _.cloneDeep(sellCarParts);
const reviewsContent = _.cloneDeep(reviews);
const usersContent = _.cloneDeep(users);
const reviewsList = [];
const proReviewsList = [];

reviewsContent.items.forEach((review) => {
    const userInfo = usersContent.items.find(user => user.ID === review.userId);
    if (userInfo.isPro) {
        proReviewsList.push({ ...review, user: userInfo });
    } else {
        reviewsList.push({ ...review, user: userInfo });
    }
});


export const IndividualCarModel = ({data: {item}, onShowImg, ImageGallery}) => {
    const carReviewData = {
        rating: item.ReviewAverage,
        reviewCount: item.ReviewCount,
    };
    return (
        <div className={styles.carModel}>
            <Container>
                <Row className={styles.ContentMargin}>
                    <Col xs={12} lg={9}>
                        <ReviewHeader
                            rating={item.ReviewAverage}
                            reviewCount={item.ReviewCount}
                            actionUrl={item.ReviewUrl}
                            actionLabel="Review this car"
                        >
                                {item.Brand.Name}, {item.Title}
                        </ReviewHeader>
                    </Col>
                    <Col xs={12} lg={3} className={styles.blockPadding}>
                        <ImageWidget thumbnailImg={item.PreviewThumbnailURL} img={item.PrimaryImageURL} onShow={onShowImg} />
                    </Col>
                </Row>
            </Container>
            <TabList>
                <Tab title="ABOUT THIS CAR" icon={<Icon name='edit_list' height={20} width={20} />}>
                    <Container>
                        <AboutCar data={item} onShowImg={onShowImg} ImageGallery={ImageGallery} />
                    </Container>
                </Tab>
                <Tab title="PRO REVIEWS (123)" icon={<Icon name='edit_list' height={20} width={20} />}>
                    <Container>
                        <CarModelReviews
                            reviews={proReviewsList}
                            rating={item.ReviewAverage}
                            reviewCount={item.ReviewCount}
                            reviewsDistribution={item.reviewsDistribution}
                            charactericsEsteem={item.charactericsEsteem}
                        />
                    </Container>
                </Tab>
                <Tab title="USER REVIEWS (123)" icon={<Icon name='edit_list' height={20} width={20} />}>
                    <Container>
                        <CarModelReviews
                            reviews={reviewsList}
                            rating={item.ReviewAverage}
                            reviewCount={item.ReviewCount}
                            reviewsDistribution={item.reviewsDistribution}
                            charactericsEsteem={item.charactericsEsteem}
                        />
                    </Container>
                </Tab>
                <Tab title="MODELS FOR SALE (123)" icon={<Icon name='key' height={20} width={20} />}>
                    <Container>
                        <CarModelBuySell data={sellCarContent} carMake="Audi" carModel="A7" />
                    </Container>
                </Tab>
                <Tab title="PARTS FOR SALE (123)" icon={<Icon name='engine' height={20} width={20} />}>
                    <Container>
                        <CarModelBuySell data={sellCarPartsContent} carMake="Audi" carModel="A7" isParts />
                    </Container>
                </Tab>
            </TabList>
        </div>
    );
};
