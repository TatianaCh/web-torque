import React from 'react';
import { Container, Button } from 'reactstrap';
import { TabList, Tab, RatingValue, ImageWidget, SimpleHeader, AboutWorkshop } from '../components';
import { workshopServices } from '../../demo/mocks/workshopServices';
import reviews from '../../demo/mocks/ReviewsResultSet.json';
import users from '../../demo/mocks/UsersResultSet.json';
import styles from '../../src/components/workshops/AboutWorkshop.scss';

const reviewsList = [];

reviews.items.forEach((review) => {
    const userInfo = users.items.find(user => user.ID === review.userId);
    reviewsList.push({ ...review, user: userInfo });
});

export const IndividualWorkshop = ({data: {item}, onShowImg}) => {
    return (
        <Container>
            <div className={styles.headerContent}>
                <div className={styles.titleBlock}>
                    <span className={styles.headerTitle}>{item.Title}</span>
                    <div className={styles.infoBlock}>
                        <div className={styles.ratingBlock}>
                            <span className={styles.rating}>{item.Rating}</span>
                            <p className={styles.ratingInfo}>overall</p>
                        </div>
                        <div className={styles.iconBlock}>
                            <RatingValue value={item.Rating} heightIcons={24} widthIcons={33} />
                            <p className={styles.views}>{item.Views} reviews</p>
                        </div>
                        <Button color="primary">WRITE A REVIEW</Button>
                    </div>
                </div>
                <div className={styles.imgBlock}>
                    <ImageWidget
                        thumbnailImg={item.PreviewThumbnailURL}
                        img={item.PrimaryImageURL}
                        onShow={onShowImg}
                    />
                </div>
            </div>
            <div>
                <AboutWorkshop data={item} reviews={reviewsList} workshopServices={workshopServices} />
            </div>
        </Container>
    );
};
