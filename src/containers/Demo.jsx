import React from 'react';
import { CardText } from 'reactstrap';
import lorem from 'lorem-ipsum';
import {
    Card,
    OverallRating, RatingValue,
    ReviewHeader, SectionHeader, SimpleHeader,
    Widget, ImageWidget,
    Icon
} from '../components';
import data from '../../demo/mocks/CarReviewResultSet.json';
import { ShowCase } from './ShowCase';

const carImage = './images/ferrari.jpg';

const models = [];

const states = {
    wip: 'Work in progress',
    style: 'Ready for styling',
    done: 'All styled and ready to go',
}

export const Demo = () => (
    <div>
        <SectionHeader>Cards</SectionHeader>
        <ShowCase name="Card" state={states.style} purpose="Show a short summary of an entity, usually in the context of search results or a list of some kind.">
            <Card
                title="Focus" subTitle="Ford"
                img={carImage} alt="Some flashy red card"
                url="#" info={(<RatingValue value={5} outOf={1} />)} >
                <CardText>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget lorem quis est malesuada sollicitudin. Cras eros augue, accumsan quis eros quis, mollis mattis dui.
                </CardText>
            </Card>
        </ShowCase>

        <SectionHeader>Headers</SectionHeader>
        <ShowCase name="SimpleHeader" state={states.style} purpose="Display a very simple page header.">
            <SimpleHeader>Some page header</SimpleHeader>
        </ShowCase>
        <ShowCase name="ReviewHeader" state={states.style} purpose="Display a page header for an object that can be reviewed">
            <ReviewHeader
                rating={3.8}
                reviewNumber={365}
                actionLabel="Review this thing"
                actionUrl="#"
                onAction={_=>alert('hello world')}>Car Make, Model</ReviewHeader>
        </ShowCase>

        <SectionHeader>Ratings</SectionHeader>
        <ShowCase name="RatingValue" state={states.style} purpose="Display a simple rating.">
            <RatingValue value={3.8} editable onChange={(rating) => alert(rating)} />
        </ShowCase>
        <ShowCase name="OverallRating" state={states.style} purpose="Display an aggregated rating.">
            <OverallRating value={3.8} reviewCount={123} />
        </ShowCase>

        <SectionHeader>Widgets</SectionHeader>
        <ShowCase name="Widget" state={states.style} purpose="Make some content stand out with a box.">
            <Widget xs={12} sm={6} md={4} lg={3} align="center">{lorem()}</Widget>
        </ShowCase>
        <ShowCase name="ImageWidget" state={states.style} purpose="Highlight an image.">
            <ImageWidget xs={12} sm={6} show={false}
                onHide={alert.bind(window)}
                onShow={alert}
                img={carImage}
                thumbnailImg={carImage}
            />
        </ShowCase>

        <SectionHeader>Others</SectionHeader>
        <ShowCase name="Icon" state={states.style} purpose="Displays an SVG icon">
            <Icon name="json" height={64} width={64} color="blue" />
        </ShowCase>
    </div>
)
