# Smart Cookie UI React Library

This library contains all the React components that will be used to render the Smart Cookie UI. This library focuses on
the look and feel of the application.

It does NOT include any integration with GraphQL or any third party webservice. Those parts will be handled in another
library that will import the `smart-cookie-ui` library.

The library will be based on Bootstrap v4. Bootstrap components are provided via the
[Reactstrap](https://reactstrap.github.io/) library.

## Setting up your development environement
This assumes you have `git` and `npm` installed on your work station.

```bash
git clone git@bitbucket.org:webtorque-dev/smart-cookie-ui.git
cd smart-cookie
npm install
```

To build the library you can run `npm run build`.

## Running the demo project
The project comes with a demo project. The demo project is just there as a show case and to test the UI components with
mock data. It's not included in the compiled library.

To run the demo project execute `npm run watch`. This will fire up browser-sync and the demo project will be accessible
on [http://localhost:3000](http://localhost:3000/)

The root of the demo project showcases individual components to quickly demonstrate what they should look like.

The other pages on the demo project will combine components together to replicate the mock ups.

## Package structure

* `demo`: Contains the demo project.
  * `Components`: Those are just some dummy components use to showcase the library. They are not included in the built
  library.
  * `images`: Just some dummy images.
  * `Mock`: Contains some mock data used to populate the showcase components.
  * `index.html and main.jsx`: Entry point for the demo project.
  * `routes.json and subRoutes.json`: List of links use to generate the navigations for the demo project.
* `lib`: Contains hte compile library.
* `src`: Source code for the library.
  * Components Contains React components. The source folder is divided into sub-folders but the components themselves
  are exported into a flat hierachy.
  * `Config`: Contains basic configuration elements.
  * `icons`: Contains SVG icons. Right now there's only a single dummy icon there, but we'll had more there over time.
  Those icons are meant to tie in with the `<Icon>` component.
  * `styles`: contains some style sheet in SCSS files.

  * `Utilites`: Contains utility functions that can be reuse by components.

## Component structure
Smart Cookie UI Components are stored unser `src/Components/`. The components are divided up into folders for
convenience. Most component have a related `SCSS` file that contains styles specific to them. Those styles are imported
in the JSX file.

The Card compoent (`src/Components/Card/Card.jsx`) has been developed to a greater extend to serve as an example. Other
component should look fairly similar.

## Mock Data
Since this library is meant to be UI only, we included some mock data in the demo project under `demo/Mock`. This mock
data can be used to populate the component in the demo.

## Global Styles
Some styles that apply accross the entire project are contained in `src/styles`. Currently, there's only two files
there:
* `bootstrap.scss`, contains a generic styles, mostly use to override bootstrap styles that applies to all components.
The demo project directly loads this file.
* `global.scss`, contains SASS variables that are shared by `bootstrap.scss` and components styles.

If you want to override default bootsrap SASS variables, you can do so in `global.scss`. Many of the component specific
SASS files will also import `global.scss` to reference some variables.

Feel free to adjust these files as need be or to add other ones.

## Icons
There's a simple SVG icon component in `src/Components/Icon/Icon.jsx`. Here's an example about how to use it:
```html
<Icon
    name="json"
    size={64}
    color="blue" />
```

`name` needs to correspond to a file in `src/icons/`. `size` is the dimension of the icon in pixels. `color`
needs to be a class define in `src/Components/Icon/Icon.scss` and can be use to change the color of the icon on the fly.

Right now there's only one dummy icon called `json`. To add more, copy the file to the `icons` folder and add a
reference to it in `src/icons/index.js`.

You can also use Font-Awesome where it's convenient.
```js
import Fa from 'react-fontawesome';

export bankIcon = () => (<Fa name="bank" />)
```
