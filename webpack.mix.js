
const mix = require('laravel-mix');
const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const historyFallback = require('connect-history-api-fallback');

const config = {
    module: {rules: [{
        test: /\.svg$/,
        use: [
            {
                loader: "babel-loader"
            },
            {
                loader: "react-svg-loader",
                options: {
                    jsx: true // true outputs JSX tags
                }
            }
        ]
    }]}
}

if (process.env.LARAVEL_MIX_WATCH !== undefined) {
    // If we're calling this with the --watch flag, build the demo project
    mix
        .setPublicPath('srv')
        .react('src/main.jsx', 'srv/main.js').sourceMaps()
        .sass('src/styles/bootstrap.scss', 'srv/main.css').options({ processCssUrls: false }).sourceMaps()
        .copy('src/styles/react-select.css', 'srv/react-select.css')
        .copy('src/index.html', 'srv/index.html')
        .copy('src/styles/fonts', 'srv/fonts')
        .copy('demo/images', 'srv/images')
        .copy('src/images', 'srv/images')

    config.plugins = [
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: {
                baseDir: ['srv'],
                middleware: [historyFallback()]
            }
        })
    ];

} else {
    // If we're calling this without the --watch option let's build the library.


    if (process.env.LARAVEL_MIX_BUILD_CSS_ONLY) {
        mix
            .setPublicPath('lib')
            .sass('src/styles/bootstrap.scss', 'lib/smart-cookie-ui.css').options({ processCssUrls: false }).sourceMaps()
            .copy('src/styles/fonts', 'lib/fonts')
            .copy('src/images', 'lib/images')
    } else {
        mix
            .setPublicPath('lib')
            .react('src/index.js', 'lib/smart-cookie-ui.js').sourceMaps()
            .minify('lib/smart-cookie-ui.js')

        config.entry = __dirname + '/src/index.js';
        config.output = {
            library: 'smart-cookie-ui',
            libraryTarget: 'umd',
            umdNamedDefine: true,
            path: __dirname + '/lib',
            filename: 'smart-cookie-ui.js'
        };
    }


}

mix.webpackConfig(config)


global.Mix.listen('configReady', (config) => {
    config.module.rules
        .filter(rule => rule.test != undefined && rule.test.test != undefined && rule.test.test('.scss'))
        .forEach(rule => {
            rule.loaders = [
                'style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        'modules': true,
                        localIdentName: '[name]__[local]',
                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                      plugins: function () {
                        return [
                          require('autoprefixer')({
                            browsers: [
                              '>1%',
                              'last 3 versions',
                              'Firefox ESR',
                              'not ie < 9', // React doesn't support IE8 anyway
                            ]
                          })
                        ];
                      }
                    }
                },
                'sass-loader'
            ]
        });
    config.module.rules
        .filter(rule => rule.test != undefined && rule.test.test != undefined && rule.test.test('.ttf'))
        .forEach(rule => {
            rule.test = /\.(woff2?|ttf|eot|otf)$/
        });
})
