
export const brands = [
    {ID: 1, Name: "Acura"},
    {ID: 2, Name: "Alfa Romeo"},
    {ID: 3, Name: "Aptera"},
    {ID: 4, Name: "Aston Martin"},
    {ID: 5, Name: "Audi"},
    {ID: 6, Name: "Austin"},
    {ID: 7, Name: "Bentley"},
    {ID: 8, Name: "BMW"},
    {ID: 9, Name: "Bugatti"},
    {ID: 10, Name: "Buick"},
    {ID: 11, Name: "Cadillac"},
    {ID: 12, Name: "Chevrolet"},
    {ID: 13, Name: "Chrysler"},
    {ID: 14, Name: "Citroën"},
    {ID: 15, Name: "Corbin"},
    {ID: 16, Name: "Daewoo"},
    {ID: 17, Name: "Daihatsu"},
    {ID: 18, Name: "Dodge"},
    {ID: 19, Name: "Eagle"},
    {ID: 20, Name: "Fairthorpe"},
    {ID: 21, Name: "Ferrari"},
    {ID: 22, Name: "FIAT"},
    {ID: 23, Name: "Fillmore"},
    {ID: 24, Name: "Foose"},
    {ID: 25, Name: "Ford"},
    {ID: 26, Name: "Geo"},
    {ID: 27, Name: "GMC"},
    {ID: 28, Name: "Hillman"},
    {ID: 29, Name: "Holden"},
    {ID: 30, Name: "Honda"},
    {ID: 31, Name: "HUMMER"},
    {ID: 32, Name: "Hyundai"},
    {ID: 33, Name: "Infiniti"},
    {ID: 34, Name: "Isuzu"},
    {ID: 35, Name: "Jaguar"},
    {ID: 36, Name: "Jeep"},
    {ID: 37, Name: "Jensen"},
    {ID: 38, Name: "Kia"},
    {ID: 39, Name: "Lamborghini"},
    {ID: 40, Name: "Land Rover"},
    {ID: 41, Name: "Lexus"},
    {ID: 42, Name: "Lincoln"},
    {ID: 43, Name: "Lotus"},
    {ID: 44, Name: "Maserati"},
    {ID: 45, Name: "Maybach"},
    {ID: 46, Name: "Mazda"},
    {ID: 47, Name: "McLaren"},
    {ID: 48, Name: "Mercedes-Benz"},
    {ID: 49, Name: "Mercury"},
    {ID: 50, Name: "Merkur"},
    {ID: 51, Name: "MG"},
    {ID: 52, Name: "MINI"},
    {ID: 53, Name: "Mitsubishi"},
    {ID: 54, Name: "Morgan"},
    {ID: 55, Name: "Nissan"},
    {ID: 56, Name: "Oldsmobile"},
    {ID: 57, Name: "Panoz"},
    {ID: 58, Name: "Peugeot"},
    {ID: 59, Name: "Plymouth"},
    {ID: 60, Name: "Pontiac"},
    {ID: 61, Name: "Porsche"},
    {ID: 62, Name: "Ram"},
    {ID: 63, Name: "Rambler"},
    {ID: 64, Name: "Renault"},
    {ID: 65, Name: "Rolls-Royce"},
    {ID: 66, Name: "Saab"},
    {ID: 67, Name: "Saturn"},
    {ID: 68, Name: "Scion"},
    {ID: 69, Name: "Shelby"},
    {ID: 70, Name: "Smart"},
    {ID: 71, Name: "Spyker"},
    {ID: 72, Name: "Spyker Cars"},
    {ID: 73, Name: "Studebaker"},
    {ID: 74, Name: "Subaru"},
    {ID: 75, Name: "Suzuki"},
    {ID: 76, Name: "Tesla"},
    {ID: 77, Name: "Toyota"},
    {ID: 78, Name: "Volkswagen"},
    {ID: 79, Name: "Volvo"},
];
