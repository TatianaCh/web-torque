
export const workshopServices = [
    {ID: 1, Name: "WoF - Warrent of Fitness"},
    {ID: 2, Name: "Car Servicing"},
    {ID: 3, Name: "Brakes"},
    {ID: 4, Name: "Repairs"},
    {ID: 5, Name: "Engine Management and Diagnostics"},
    {ID: 6, Name: "Tyres, Wheel Alignment and Puncture"},
];
