import React from 'react';
import * as Rambda from 'rambda';
import { DropDown, TextField, RangedDropDown } from '../../src/components';
import { NumericRangedDropDown } from '../../src/components';
import { brands } from './brands';
import { Input } from 'reactstrap';
import { years } from './years';
import bodyTypes from './bodyTypes.json';

const dropDownProps = {
    valueField: 'ID',
    displayField: 'Name',
    labelHidden: true,
    placeholder: 'Any'
}

const priceOptions = Rambda.times( i => (
    { ID: i * 1000, Name: i * 1000 }
), 20);

export const CarFilters = [
    {label: 'Make', field: (<DropDown {...dropDownProps} label="Make" items={brands} />) },
    {label: 'Model', field: (<TextField label="Model" name="model" labelHidden placeholder="" />) },
    {label: 'Year', field: (<NumericRangedDropDown labelHidden min={1910} max={2017} />) },
    {label: 'Body Type', field: (<DropDown {...dropDownProps} label="Body Type" items={bodyTypes} />) },
    {label: 'Price', field: (<RangedDropDown labelHidden items={priceOptions} />) },
]
