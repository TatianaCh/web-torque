import _ from 'lodash';


export const years =
    _.range(2018, 1900)
    .map(year => ({"ID": year, "Name": year}));
