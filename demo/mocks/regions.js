export const regions = [
    {ID: 1, Name: "Region 1"},
    {ID: 2, Name: "Region 2"},
    {ID: 3, Name: "Region 3"},
    {ID: 4, Name: "Region 4"},
    {ID: 5, Name: "Region 5"},
    {ID: 6, Name: "Region 6"},
    {ID: 7, Name: "Region 7"},
    {ID: 8, Name: "Region 8"},
];
