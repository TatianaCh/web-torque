export const eventTypes = [
    {ID: 1, Name: "Track"},
    {ID: 2, Name: "Race"},
    {ID: 3, Name: "Festival"},
    {ID: 4, Name: "Presentation"},
];
